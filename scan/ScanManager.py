import json
import sys
import pika

class Counter(object):
    def __init__(self):
        self.count = 0
    def inc(self):
        self.count += 1
    def print(self):
        print("Count: ", self.count)
debug = False

arg1 = {"Product ID": "P17203110", "Date Added": "10/31/2017", "Manufacturer": "Medtronic", "Product Name": ["Diabetes Tester"], "Type": "['External Device','Diabetes','Tester']", "Description": "This device allows users to monitor their blood glucose level."}
arg2 = {"Product ID": "P15202810", "Date Added": "10/28/2015", "Manufacturer": "Medtronic", "Product Name": ["Heart Monitor"], "Alias": "EKG", "Type": "['External Device','Heart','Monitor']", "Description": "This device allows monitoring on heart beat."}
arg4 = {"Product Name": "hertmonitor"}
arg3 = {"Product ID": "P16202301", "Date Added": "01/23/2016", "Manufacturer": "Medtronic", "Product Name": ["Pace Maker"], "Description": "This device is embedded into a client, it keeps their heart beating in a regular rhythm using small electrical pulses."}
args = [arg1, arg2, arg3]#, arg4]
jsonObjects = []

count = Counter()

for arg in args:
    jsonObjects.append(json.dumps(arg))

# USED TO TRACK TOTAL NUMBER OF REQUESTS
totalRequests = len(jsonObjects)

def initialize_rabbitmq():
    # The following two lines create a connection at 'localhost' and open a channel from that connection.
    con = pika.BlockingConnection(pika.ConnectionParameters('localhost'))
    chan = con.channel()

    # Declares a queue 'scan_requests'. This queue will be created if it does not already exist. This
    # is the queue to which the "incoming requests" (i.e. requests coming from control) will be sent
    chan.queue_declare(queue='scan_requests')
    # Now we need a new queue 'results_queue' this will hold all the results produced by a scanner.
    chan.queue_declare(queue='results_queue')
    return con, chan

def publish_requests():
    # Assuming we have received all incoming requests, we then publish each request as a message to
    # the 'scan_requests' queue.
    for json in jsonObjects:
        # Here we build the body of the message. This is done by appending the json string to the
        # currently empty message. JSON is a string when we set message to json
        message = json
        # Then we publish the message.
        #   exchange does ? TODO Research exchange (basic_publish)
        #   routing_key sends it to the specified queue
        #   body sets the body of the message
        chan.basic_publish(exchange='',
                              routing_key='scan_requests',
                              body=message)
        #finally we print a confirmation that the message was enqueued.
        print(" [x] Sent %r" % message)


con, chan = initialize_rabbitmq()

# Last we close the outbound connection
publish_requests();
# WORKS!! PUBLISHES THESE TASKS TO THE QUEUE (THEY ARE CURRENTLY JUST JSON OBJECTS)


def callback(ch, method, properties, body):
    count.inc()
    count.print()
    #if debug:
    #print(" [x] Recieved %r" % body)
    text_file = open("Output.txt", "a")
    text_file.write(body.decode("utf-8", "strict") + '\n')
    text_file.close()
    ch.basic_ack(delivery_tag=method.delivery_tag)


chan.basic_consume(callback, queue='results_queue')
chan.start_consuming()