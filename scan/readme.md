# Ebay Scan Service

The scan service consists of two *scanner* script, a *manager* script, and a *configuration* file.

#### Config File:

The **ebay.yaml** file contains the configuration parameters for contacting the ebay servers. It contains:
* Two appids
  * One of these allows the scanners to contact eBay's sandbox environment.
  * The other allows the scanners to contact the production shopping, finding, and trading apis.
* Two certids
  * One for production
  * One for sandbox
* A devid


#### Scan Manager:
The **ScanManager.py** script enables testing without the use of the control engine from the control part
of this project. Running this script will add four sample requests to the requests_queue, so that a scanner
script can pick them up and process them. 

Once this queue as been filled, this script will wait until a scanner publishes results to the results queue.
It will take each result out of the results queue and will print each result to the console.

To run the scan manager simply type:
```angular2html
python3 ScanManager.py
```


#### Scanner Scripts:
Included are two scanner scripts:
* eBayScanner.py
  * This script searches eBay for each item in the requests queue. This scanner does no filtering of the
    results that it obtains.
* FilteredeBayScanner.py
  * This script searches eBay for each item in the requests queue. It will filter out products that do
    not pertain to the term being searched. 
  
  
To run a scanner simply type "python3 *scanner_name*"

So to run either of the two examples included in this project:
```angular2html
python3 eBayScanner.py or python3 FilteredeBayScanner.py
```

Each scanner also has a list of command line options. To view these options use the argument
-h.
```angular2html
python eBayScanner.py -h
python FilteredeBayScanner.py -h
```

#### Warning
Currently neither the scanner scripts, nor the scan manager script close themselves. You must ctrl+c to quit
them when they are done processing. This is because each stays open listening to a queue for work to do. This
will be fixed soon.