"""

This version of the eBay Scanner will contact the eBay API, to search for specified products.
It will filter out various terms from the results. These filters are discussed further down.

Command-Line Arguments
=======================
+-----------+-------+-----------+--------------+-----------------+-------------------------------------------------------------------+
| Option    | Short | Long      | Parameter    | Default         |Description                                                        |
+===========+=======+===========+==============+=================+===================================================================+
| Help      | -h    | N/A       | N/A          | N/A             | Displays options info.                                            |
+-----------+-------+-----------+--------------+-----------------+-------------------------------------------------------------------+
| Server    | -s    | --rabbitmq| <server>     | 'localhost'     | Allows you to specify the server where the MQ service is running. |
+-----------+-------+-----------+--------------+-----------------+-------------------------------------------------------------------+
| Requests  | -q    | --request | <queue_name> | 'scan_requests' | Allows you to specify the queue from which the script pulls reqs. |
+-----------+-------+-----------+--------------+-----------------+-------------------------------------------------------------------+
| Results   | -r    | --result  | <queue_name> | 'results_queue' | Allows you to specify the queue to which the script publishes.    |
+-----------+-------+-----------+--------------+-----------------+-------------------------------------------------------------------+
| Debug     | -d    | --debug   | N/A          | False           | Sets the script to trigger certain debug statements.              |
+-----------+-------+-----------+--------------+-----------------+-------------------------------------------------------------------+
| Verbose   | -v    | --verbose | N/A          | False           | Publishes all obtained requests and results to a log file. NOT IMP|
+-----------+-------+-----------+--------------+-----------------+-------------------------------------------------------------------+

First
    The script calls :func:`scan.FilteredeBayScanner.default_mq_connections` which initializes all rabbitMQ connection
    parameters to their default values. Also :func:`scan.FilteredeBayScanner.default_settings` is called to initialize
    the debug and verbose parameters to their default values. Any command line options are parsed next and the prior
    parameters are updated accordingly.
Next
    A connection and channel are established with the rabbitMQ service running on server. This is done with a call to
    :func:`scan.FilteredeBayScanner.initialize_rabbitmq`. :func:`scan.FilteredeBayScanner.channel.start_consuming` begins
    serving the requests from the requests queue. Each time a request is obtained from the queue
    :func:`scan.FilteredeBayScanner.callback` is called, which passes the requested item to
    :func:`scan.FilteredeBayScanner.contact_ebay`.
Finally
    A connection to the eBay API is established, and :func:`scan.FilteredeBayScanner.apply_filters` adds filters to the
    search term. A request is made to eBay's servers and the results are put in the results queue for the controller to
    process.

.. moduleauthor:: Alex Foreman <alex.foreman@yahoo.com>

"""

import datetime
import json
import sys
import getopt
from ebaysdk.exception import ConnectionError
from ebaysdk.finding import Connection
import pika


def apply_filters(item):
    """

    Given an item/product from the database, apply filtering terms to the string to cut out bad results.

    :param item: A product
    :type item: str
    :return item: Updated string including the terms used to filter results.
    :rtype item: str


    """
    item += ", Medtronic -watch* -band* -wrist* -belt* -phone* -case -sport* -shirt*"
    return item


# INITIALIZE THE RABBITMQ QUEUES AND STUFF
def initialize_rabbitmq(host='localhost', req_queue='scan_requests', res_queue='results_queue'):
    """

    Given connection parameters, establish a connection to the rabbitMQ service. From this connection
    establish a channel, and initialize two queues.

    :param host: The name of server on which the rabbitMQ service is running.
    :type host: str
    :param req_queue: The name of the queue from which the script should obtain scan requests.
    :type req_queue: str
    :param res_queue: The name of the queue from which the script should publish results.
    :type res_queue: str
    :return chan: The channel object obtained from the rabbitMQ service. This channel holds both the request and result queue.
    :rtype chan: :class:`BlockingChannel`
    :return con: The connection object obtained from the rabbitMQ service. This connection holds the returned channel.
    :rtype con: :class:`BlockingConnection`

    """
    if debug: # TODO move the debug statement to print to a log file
        print("-- Initializing rabbitMQ parameters based on run arguments --")
        print(" Using server - ", host)
        print(" The request queue's name is ", req_queue)
        print(" The results queue's name is ", res_queue)
        print()
    # First we need to get a Connection and channel on which to get requests from
    con = pika.BlockingConnection(pika.ConnectionParameters(host))
    chan = con.channel()

    # Now we need to declare the 'scan_requests' so that if this is started before the manager
    # (IT SHOULD NOT BE), this script won't crash.
    chan.queue_declare(req_queue)
    chan.queue_declare(res_queue)
    return chan, con


def default_mq_connections():
    """

    Returns the default rabbitMQ connection parameters

    :return default_server: The default name of the server from which to connect to the rabbitMQ service.
    :rtype default_server: str
    :return default_requests: The default name of the queue from which to obtain scan requests.
    :rtype default_requests: str
    :return default_results: The default name of the queue to be used for publishing results.
    :rtype default_results: str

    """
    default_server = 'localhost'
    default_requests = 'scan_requests'
    default_results = 'results_queue'
    #if debug: # TODO move the debug statement to print to a log file
    print("-- Setting the default rabbitMQ connection parameters --")
    print(" The default server is set to ", default_server)
    print(" The default requests queue is set to ", default_requests)
    print(" The default results queue is set to ", default_results)
    print()
    return default_server, default_requests, default_results


def default_settings():
    """

    Returns the default state for the debug and verbose options.

    :return default_debug: The default state for the debug option.
    :rtype default_debug: bool
    :return default_verbose: The default state fore the verbose option.
    :rtype default_verbose: bool

    """
    default_debug = False
    default_verbose = False
    #if debug: # TODO move the debug statement to print to a log file
    print("-- Setting the default run options --")
    print(" The default debug option is false")
    print(" The default verbose option is false")
    return default_debug, default_verbose


def print_help_menu():
    """

    Prints the help menu, displaying the list of valid command line arguments for this script.

    """
    print(' |-------------------------------------------------------------------------------------------|', '\n',
          '                                  ', sys.argv[0], ' help                                    ', '\n',
          '|-------------------------------------------------------------------------------------------|', '\n',
          '| Options:                                                                                  |', '\n',
          '|     -h    HELP                  Displays this info.                                       |', '\n',
          '|     -s --rabbitmq   <server>    Allows you to specify the server on which the MQ service  |', '\n',
          '|                                 is running. By default this is "localhost"                |', '\n',
          '|     -q --request   <queue_name> Allows you to specify the name of the the queue from which|', '\n',
          '|                                 this script will pull requests. By default this queue is  |', '\n',
          '|                                 "scan_requests".                                          |', '\n',
          '|     -r --result    <queue_name> Allows you to specify the name of the queue to which this |', '\n',
          '|                                 script will publish results. By default this queue is     |', '\n',
          '|                                 "results_queue".                                          |', '\n',
          '|     -d --debug                  Sets the script to trigger certain debug statements.      |', '\n',
          '|     -v --verbose                Publishes all obtained requests and results to a log file.|', '\n',
          '|-------------------------------------------------------------------------------------------|')


def contact_ebay(item):
    """

    At a high level this function obtains the product name from the item, opens a connection to the eBay API, applies
    filtering terms to the item, makes a request using the item and filters, then publishes each item in the response
    to the response queue.


    :param item: The json formatted string that was obtained from the request queue.
    :type item: str


    """
    item = json.loads(item)
    given_item = item
    # here we try to grab an item from the request, if there is none we just return.
    try:
        product = item['Product Name']
    except AttributeError as e:
        print("AttributeError!!!")
        print("We expected to find an item with attribute 'Product Name'!")
        print('\nHere is the error:')
        print(e)
        return
    # here we try to grab a manufacturer from the request, if there is none we print info and
    # carry on.
    try:
        manufacturer = item['manufacturer']
    except KeyError as e:
        if debug:  # TODO move the debug statement to print to a log file
            print("No manufacturer specified for item: ", product)
            print("Continuing as normal...")
    # Here we are trying to search the api for 'product' we assert that we got a response. If we get a connectionerror
    # we print the error and move on.
    try:
        api = Connection()
        # The next line applies additional search tearms to the string, used for filtering out junk.
        search_term = apply_filters(product)
        response = api.execute('findItemsAdvanced', {'keywords': search_term})
        assert (response.reply.ack == 'Success')
        assert (type(response.reply.timestamp) == datetime.datetime)

        # If we don't get any results back we should immediately return.
        if response.reply.paginationOutput.totalEntries == '0':
            return

        # For each page grab all results and publish them.
        for page in range(int(response.reply.paginationOutput.totalPages)):
            if debug:  # TODO move the debug statement to print to a log file
                print("----------- PAGE ", page + 1, "-------------")
            items = []
            # Here we try to grab an item. After 100 pages we get an error. When fields are pulled from this error
            # we get an AttributeError. So we trap it, print it, and move on.
            try:
                # We add every item we find to the items list.
                for item in response.reply.searchResult.item:
                    # if debug:  TODO move the debug statement to print to a log file
                        # TODO Update this to not use counter, but instead rely on a field from item
                    current_result = item
                    product_name = current_result.title
                    product_url = current_result.viewItemURL
                    product_category = current_result.primaryCategory.categoryName

                    result_dict = {"Name": product_name,
                                   "URL": product_url,
                                   "Category": product_category}

                    # Add the calling dict into the item.
                    result_dict = dict(result_dict, **given_item)
                    result_string = json.dumps(result_dict)
                    items.append(result_string)
            except AttributeError as e:
                if debug:  # TODO move the debug statement to print to a log file
                    print(e)
                break
            # Now publish each item from this page to the results_queue
            for product in items:
                    publish_results(product)
            # Finally go to the next page if it is not the last page
            if page < int(response.reply.paginationOutput.totalPages) - 1:
                api.next_page()
                response = api.response
    except ConnectionError as e:
        if debug:  # TODO move the debug statement to print to a log file
            print(e)
            print(e.response.dict())


# PUBLISH RESULTS ONCE THEY ARE OBTAINED
def publish_results(body):
    """

    Publishes each result found to the results queue.

    :param body: This contains all of the information of a result found. Including URL, Searched Item, Product Name, original string, etc.
    :type body: str

    """
    if debug: # TODO move the debug statement to print to a log file
        print(" [*] Result Found - %r" %body)
    channel.basic_publish(exchange='',
                          routing_key=out_queue,
                          body=body)


# DO THIS STUFF AND SEND AN ACK WHEN DONE
def callback(ch, properties, method, body):
    """

    Callback is invoked any time a message is pulled from the requests queue. The body of the message is parsed, and
    and encoded into a utf-8 string (this is done because the body is initially just a byte string). Then it is converted
    into a python dictionary, so it can then be formatted into a json formatted string. Finally it is passed along to
    :func:`scan.FilteredeBayScanner.contact_ebay`.

    :param ch: The channel which contains the queues that the script is connected to.
    :type ch: :class:`BlockingChannel`
    :param properties: Basic properties of the channel
    :type properties: :class:`Basic.Deliver`
    :param method:
    :type method: :class:`BasicProperties`
    :param body: The information of the message. In this case it is request strings.
    :type body: bytes


    """
    # Decode Message into Dict
    if debug: # TODO move the debug statement to print to a log file
        print(" [x] Recieved %r" % body)
    b_string = body
    string = b_string.decode("utf-8", "strict")
    prod_as_dict = json.loads(string)  # loads converts 'string' to a dict (for reading)
    item = json.dumps(prod_as_dict)
    contact_ebay(item)


# Main function, if the program is ran directly, run this passing arguments
#   Possible arguments are:
#     -h for help
#     -s for rabbitmq server location
#     -q for input queue
#     -r for output queue
if __name__ == '__main__':
    # this gets the passed in arguements
    argv = sys.argv[1:]

    # sets server in-out queue as the defaults
    server, in_queue, out_queue = default_mq_connections()

    # sets debug and verbose to false
    debug, verbose = default_settings()
    #tries to get the options
    try:
        opts, args = getopt.getopt(argv, "hs:q:r:dv", ["rabbitmq=", "request=", "result=", "debug", "verbose"])
    except getopt.GetoptError:
        # if not options run the file using the inputqueue as the requests_queue
        print('Invalid options selected. Use option -h to see a list of valid options as well as their functionality.')
        sys.exit()
    for opt, arg in opts:
        if opt == '-h':
            print_help_menu()
            sys.exit()
        elif opt in ("-s", "--rabbitmq"):
            server = arg
        elif opt in ("-q", "--request"):
            in_queue = arg
        elif opt in ("-r", "--result"):
            out_queue = arg
        elif opt in ("-d", "--debug"):
            debug = True
        elif opt in ("-v", "--verbose"):
            verbose = True

    # Obtain a channel and connection
    channel, connection = initialize_rabbitmq(server, in_queue, out_queue)

    # Ensure that consumers only take 1 request at a time
    channel.basic_qos(prefetch_count=0)

    # Set this program to consume from the in_queue. It will call
    # the callback function when it takes from the in_queue.
    channel.basic_consume(callback, queue=in_queue, no_ack=True)

    # consume!
    channel.start_consuming()
