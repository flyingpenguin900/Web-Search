from flask import Flask, render_template, json, request, jsonify
from flaskext.mysql import MySQL
from werkzeug import generate_password_hash, check_password_hash
import datetime
import time
import json


app = Flask(__name__)

# Get's scan info from a POST request
# Returns Dictionary
def get_scan_info(request):
    # get scan variables
    name = request.form['scanName']
    description = request.form['scanDescription']
    start = request.form.get('startDate')
    repeat = request.form.get('frequency')
    gap_days = 0
    gap_weeks = 0
    gap_months = 0
    gap_time = 0
    
    # get intervals if recurring scan
    if repeat == '1':
        end = request.form.get('endDate')
        interval = request.form.get('interval')
        if interval == 'Hourly':
            gap_time = '00:60:00'
        if interval == 'Daily':
            gap_days = 1
        if interval == 'Weekly':
            gap_weeks = 1
        if interval == 'Biweekly':
            gap_weeks = 2
        if interval == 'Monthly':
            gap_months = 1
        repeat = True
    else:
        end = 0
        repeat = False
        
    # get search items
    scan_items = []
    all_products = request.form.get('scan_items')
    if all_products == '1':
        all_products = True
    else:
        all_products = False
        items = request.form.getlist('search_item')
        for item_id in items:
            scan_items.append(item_id)
    
    # get scan methods
    scan_methods = []
    methods = request.form.getlist('scan_method')
    for i in methods:
        scan_methods.append(i)

    # get notification methods
    notifications = []
    notification_method = request.form.getlist('notification_method')
    for i in notification_method:
        notifications.append(i)
       
    # create JSON object
    data = {
        "Name": str(name),
        "Description": str(description),
        "Start": start,
        "End": end,
        "Repeat": repeat,
        "Gap_Days": gap_days,
        "Gap_Weeks": gap_weeks,
        "Gap_Months": gap_months,
        "Gap_Time": gap_time,
        "All_Products": all_products,
        "Items": scan_items,
        "Methods": scan_methods,
        "Notifications": notifications
    }

    #json_data = json.dumps(data)
    print(data)
    return data
        

# Helper function to get past 7 days fromn current date
# Returns a list of dates, in order
def last_week():
    week = []
    today = datetime.date.today()
    start_delta = datetime.timedelta(days=6)
    start_of_week = today - start_delta
    for day in range(7):
        week.append(start_of_week + datetime.timedelta(days=day))
    return week

# Helper function for converting a date-time object to unix timestamp
def dt_to_timestamp(dt):
    epoch = datetime.datetime.utcfromtimestamp(0)
    date_processing = dt.replace('T', '-').replace(':', '-').split('-')
    date_processing = [int(v) for v in date_processing]
    date_out = datetime.datetime(*date_processing)
    return int((date_out - epoch).total_seconds())


# Helper function for finding the next weekday timestamp for a given weekday following given start date
# start = date object
# weekday:
#    0 = Monday
#    1 = Tuesday
#    2 = Wednesday
#    3 = Thursday
#    4 = Friday
#    5 = Saturday
#    6 = Sunday
def next_weekday(start_ts, weekday):
    # convert to GMT time zone
    gmt_zone =  pytz.timezone('GMT')
    start = datetime.datetime.fromtimestamp(start_ts, gmt_zone)
    # strip date
    start_time = start.time()
    print ("Start time: "+ str(start_time))
    start = start.date()
    days_ahead = weekday - start.weekday()
    if days_ahead < 0: # Target day already happened this week
        days_ahead += 7
    next = start + datetime.timedelta(days_ahead)
    print ("Next day: "+ str(next))
    # add time back
    next_ts = datetime.datetime.combine(next, start_time)
    print ("Next day/time: "+ str(next))
    return next_ts
