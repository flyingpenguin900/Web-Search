from flask import Flask, render_template, json, request, jsonify
from flaskext.mysql import MySQL
from werkzeug import generate_password_hash, check_password_hash
import datetime
import time

app = Flask(__name__)

mysql = MySQL()

# MySQL configurations
app.config['MYSQL_DATABASE_USER'] = 'master'
app.config['MYSQL_DATABASE_PASSWORD'] = 'NewAWSPassword1234'
app.config['MYSQL_DATABASE_DB'] = 'webscanner'
app.config['MYSQL_DATABASE_HOST'] = 'web-search.cmxgj4wqbqnn.us-west-2.rds.amazonaws.com'
mysql.init_app(app)



##### Scanner Functions ####

'''
Create Scanner
Parameters:
    - Scanner dictionary with the following key/values:
        'name'
        'description'
        'working_directory'
        'path'
Returns:
    -   New scanner's ID or None if failed
'''
def create_scanner(scanner):
    # get current date/time
    date_added = datetime.datetime.now()
    command = "INSERT INTO Scanner (Name, Description, Path, Working_Directory, Date_Added) VALUES ('%s', '%s', '%s', '%s', '%s')" % \
        (str(scanner['name']), str(scanner['description']), str(scanner['path']), str(scanner['working_directory']), str(date_added))
    # create scanner
    try: 
        conn = mysql.connect()
        cursor = conn.cursor()
        cursor.execute(command)
        conn.commit()
        scanner_id = cursor.lastrowid
        print("---> Successfully created user. Scanner_ID: " + str(scanner_id))
        conn.close()
        return True
    except:
        print("---> Failed to create scanner")
        conn.close()
        return False


'''
Delete Scanner
Parameters:
    - Scanner ID
Returns:
    - True (success) or False (fail)
'''
def delete_scanner(scanner_id):
    try:
        conn = mysql.connect()
        cursor = conn.cursor()
        cursor.execute("UPDATE Scanner SET Deleted = True WHERE Scanner_ID = " + str(scanner_id))
        conn.commit()
        print("---> Successfully deleted Scanner_ID: " + str(scanner_id))
        conn.close()
        return True
    except:
        print("---> Failed to delete Scanner_ID: " + str(scanner_id))
        conn.close()
        return False
    



'''
Update Scanner
Parameters:
    - Scanner dictionary with values to update
Returns:
    - True (success) or False (fail)
'''
def update_scanner(scanner):
    try:
        # update scanner
        conn = mysql.connect()
        cursor = conn.cursor()
        count = cursor.execute("UPDATE Scanner SET Name = '%s', Description = '%s', Path = '%s', Working_Directory = '%s' WHERE Scanner_ID = %s " % 
            (str(scanner['name']), str(scanner['description']), str(scanner['path']), str(scanner['working_directory']), scanner['scanner_id'])) 
        conn.commit()
        print("---> Successfully updated Scanner_ID: " + str(scanner['scanner_id']))
        conn.close()
        return True

    except:
        print("Failed to update Scanner_ID: " + str(scanner['scanner_id']))
        conn.close()
        return False




'''
Get User
Parameters:
    - Scanner ID
Returns:
    - Dictionary with scanner info or None if fail
'''
def get_scanner(scanner_id):
    try:
        conn = mysql.connect()
        cursor = conn.cursor()
        cursor.execute("SELECT * FROM Scanner WHERE Scanner_ID = " + str(scanner_id))
        results = cursor.fetchall()
        scanner = {}
        for row in results:
            scanner = {
                'scanner_id' : str(row[0]),
                'name' : str(row[1]),
                'description' : str(row[2]),
                'path' : str(row[3]),
                'date_added' : str(row[4]),
                'working_directory' : str(row[6])
            }
        conn.close()
        return scanner
    except:
        print("Database error. Failed to get scanner.")
        conn.close()
        return None

    


'''
Get All Scanners
Parameters:
    - None
Returns:
    - List of dictionaries with scanners' info
'''
def get_scanners():
    try:
        conn = mysql.connect()
        cursor = conn.cursor()
        scanners = []
        cursor.execute("SELECT *  FROM Scanner WHERE Deleted = False")
        found_scanners = cursor.fetchall()
        for row in found_scanners:
            scanner = {
                'scanner_id' : str(row[0]),
                'name' : str(row[1]),
                'description' : str(row[2]),
                'path' : str(row[3]),
                'date_added' : str(row[4]),
                'working_directory' : str(row[6])
            }
            scanners.append(scanner)
        conn.close()
        return scanners
    except:
        conn.close()
        return None
