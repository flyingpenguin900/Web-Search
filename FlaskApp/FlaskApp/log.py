from flask import Flask, render_template, json, request, jsonify
from flaskext.mysql import MySQL
from werkzeug import generate_password_hash, check_password_hash
import datetime
import time
from time import strftime, strptime
import json
import re
import subprocess

# log classes
USER = 'user.log'
SYSTEM = 'system.log'

def log(user, message, log_type='INFO'):
    t = strftime("%Y-%m-%d %H:%M:%S,123", time.localtime())
    with open(USER, "a") as f:
        f.write(str(t) + ' - ' + str(user) + ' - ' + str(log_type) + ' - ' + str(message) + '\n')



def get_logs(log_class=[USER]):
    logs = []
    for file in log_class:

        reg = re.compile('(19|20)\\d\\d[- /.](0[1-9]|1[012])[- /.](0[1-9]|[12][0-9]|3[01]) [0-2]\\d:[0-6]\\d:[0-6]\\d,\\d\\d\\d')
        lines = []
        for line in open(file):
            if reg.search(line):
                lines.append(line)
            else:
                if len(lines) > 0:
                    lines[len(lines)-1] = lines[len(lines)-1] + '\n' + line

        for line in reversed(lines):
            log = (line.rstrip()).split('-')
            #print (log)
            logs.append({
                'date': log[0] + '-' + log[1] + '-' + (log[2].split(','))[0],
                'type': log[4],
                'user': log[3],
                'description': '-'.join(log[5:])})
    
    sort_key = lambda x:  time.mktime(strptime(x['date'], '%Y-%m-%d %H:%M:%S'))
    sorted_logs = sorted(logs, key=sort_key, reverse=True)

    return sorted_logs


def system_health():
    '''
    This gets the health in a dict.
    IMPORTANT: Assumes server system has 'systemctl' bash command!!!
    TODO: Consider using "systemctl is-active webscanner-control" or w/e
    '''

    # Get all processes from system (Assuming Ubuntu/Linux)
    system_services = subprocess.run(['systemctl'], stdout=subprocess.PIPE)
    services_ret = system_services.stdout.decode('utf-8').split('\n')

    #Filter down to only the 'scanner' services, which are hopfully ours
    # TODO: have a list of specific services to look for.
    scanner_services = list(filter(re.compile('medtronicscanner').search, services_ret))

    def process_service_strings(s):
        ''' TODO: don't assume services without spaces.
        s: 'webscanner-flask.service                                                            loaded active exited    web-serch project website'
        return {
            'name':'webscanner-flask.service',
            'status':'loaded active exited',
            'description':'web-serch project website'
        }
        '''
        split = s.split()
        return {
            'name':split[0],
            'status':' '.join(split[1:4]),
            'description':' '.join(split[4:])
        }
    health = []
    for service in scanner_services:
        health.append(process_service_strings(service))
        
    return health