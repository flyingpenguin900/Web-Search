from flask import Flask, render_template, json, request, jsonify
from flaskext.mysql import MySQL
from werkzeug import generate_password_hash, check_password_hash
import datetime
import time
import hashlib

app = Flask(__name__)

mysql = MySQL()

# MySQL configurations
app.config['MYSQL_DATABASE_USER'] = 'master'
app.config['MYSQL_DATABASE_PASSWORD'] = 'NewAWSPassword1234'
app.config['MYSQL_DATABASE_DB'] = 'webscanner'
app.config['MYSQL_DATABASE_HOST'] = 'web-search.cmxgj4wqbqnn.us-west-2.rds.amazonaws.com'
mysql.init_app(app)


##### User Functions ####


'''
Authenticate User
Parameters:
    - Email
    - Password
Returns:
    - dictionary with user ID and full name
'''
def authenticate(email, password):
    conn = mysql.connect()
    cursor = conn.cursor()
    hashed_password = hashlib.sha512(password.encode('utf-8')).hexdigest()
    try: 
        cursor.execute("SELECT * FROM Users WHERE Email = \'" + str(email) +"\'")
        results = cursor.fetchall()
        if cursor.rowcount == 0: # confirm email exists
            response = {
                'status': 'fail',
                'message' : 'Email does not exist.'
            }
        else:
            for row in results: # get user info
                user_id = str(row[0])
                user_password = str(row[4])

            if user_password != str(hashed_password): # confirm password
                response = {
                    'status': 'fail',
                    'message' : 'Invalid password.'
                }
            else:
                response = {
                    'status': 'success',
                    'full_name' : str(row[1]) + ' ' + str(row[3]),
                    'id' : str(row[0]),
                    'role' : str(row[6])
                }
    except:
        response = {
            'status': 'fail',
            'message' : 'Database error.'
        }
    conn.close()
    return response


'''
Create Notification Method for User
Parameters:
    - Notification Type (phone/e-mail)
    - Notification Value
    - User ID
Returns:
    - True (success) or False (fail)
'''
def create_notification(user_id, n_type, n_value):
    try:
        conn = mysql.connect()
        cursor = conn.cursor()
        command = "INSERT INTO Notification_Methods (User_ID, Notification_Type, Notification_Value) VALUES ('%d', '%s', '%s')" % \
            (int(user_id), str(n_type), str(n_value))
        cursor.execute(command)
        conn.commit()
        n_id = cursor.lastrowid
        print("---> Successfully created new notification. Notification_ID: " + str(n_id))
        conn.close()
        return True
    except:
        conn.rollback()
        print("---> Failed to create notification.")
        conn.close()
        return False


'''
Update Notification Method
Parameters:
    - Notification ID 
    - Notification Type (phone/e-mail)
    - Notification Value
Returns:
    - True (success) or False (fail)
'''
def update_notification(n_id, n_type, n_value):
    try:
        conn = mysql.connect()
        cursor = conn.cursor()
        count = cursor.execute("UPDATE Notification_Methods SET Notification_Type = '%s', Notification_Value = '%s' WHERE Method_ID = %s " % 
            (str(n_type), str(n_value), str(n_id))) 
        conn.commit()
        print("---> Successfully updated Notification_ID: " + str(n_id))
        conn.close()
        return True
    except:
        conn.rollback()
        print("---> Failed to update Notification_ID: " + str(n_id))
        conn.close()
        return False
    



'''
Create User
Parameters:
    - User dictionary with the following key/values:
        'first_name'
        'last_name'
        'email'
        'password'
        'role'  ('standard' or 'admin')
        'notifications'  (list of dictionaries with 'n_type' and 'n_value')
Returns:
    -   the new user's ID or None
'''
def create_user(user):
    conn = mysql.connect()
    cursor = conn.cursor()
    hashed_password = hashlib.sha512(user['password'].encode('utf-8')).hexdigest()
    command = "INSERT INTO Users (First_Name, Last_Name, Email, Password, Role) VALUES ('%s', '%s', '%s', '%s', '%s')" % \
        (str(user['first_name']), str(user['last_name']), str(user['email']), str(hashed_password), str(user['role']))
    try: # try inserting in database
        cursor.execute(command)
        conn.commit()
        user_id = cursor.lastrowid
        print("---> Successfully created user. User_ID: " + str(user_id))
        conn.close()
    except:
        print("---> Failed to create user")
        conn.close()
        return False

    # create notification methods
    for n in user['notifications']:
        create_notification(user_id, n['n_type'], n['n_value'])
    return True


'''
Delete User
Parameters:
    - User ID
Returns:
    - Dictionary with user info or None if fail
'''
def delete_user(user_id):
    try:
        conn = mysql.connect()
        cursor = conn.cursor()
        cursor.execute("UPDATE Users SET User_Deleted = True WHERE User_ID = " + str(user_id))
        conn.commit()
        print("---> Successfully deleted User_ID: " + str(user_id))
        conn.close()
        return True
    except:
        print("---> Failed to delete User_ID: " + str(user_id))
        conn.close()
        return False
    
    
    



'''
Update User
Parameters:
    - User dictionary with values to update
Returns:
    - True (success) or False (fail)
'''
def update_user(user):
    try:
        # create new notifications
        if user['new_notifications']:
            for notification in user['new_notifications']:
                create_notification(user['user_id'], notification['n_type'], notification['n_value'])

        # update existing notifications
        if user['existing_notifications']:
            for notification in user['existing_notifications']:
                update_notification(notification['n_id'], notification['n_type'], notification['n_value'])

        # update user
        conn = mysql.connect()
        cursor = conn.cursor()
        print(user)
        count = cursor.execute("UPDATE Users SET First_Name = '%s', Last_Name = '%s', Email = '%s', Role = '%s' WHERE User_ID = %s " % 
            (str(user['first_name']), str(user['last_name']), str(user['email']), str(user['role']), user['user_id'])) 
        conn.commit()
        print("---> Successfully updated User_ID: " + str(user['user_id']))
        conn.close()
        return True

    except:
        print("Failed to update user_ID: " + str(user['user_id']))
        conn.close()
        return False




'''
Get User
Parameters:
    - User ID
Returns:
    - Dictionary with user info or None if fail
'''
def get_user(user_id):
    try:
        conn = mysql.connect()
        cursor = conn.cursor()
        # get user by ID
        cursor.execute("SELECT * FROM Users WHERE User_ID = " + str(user_id))
        results = cursor.fetchall()
        user = {}
        for row in results:
            user['user_id'] = row[0]
            user['first_name'] = row[1]
            user['last_name'] = row[3]
            user['email'] = row[5]
            user['role'] = row[6]
        # get notification methods
        cursor.execute("SELECT * FROM Notification_Methods WHERE User_ID = " + str(user_id))
        results = cursor.fetchall()
        notifications = []
        for row in results:
            notification = {}
            notification['n_id'] = row[0] 
            notification['n_type'] = row[2]
            notification['n_value'] = row[3]
            notifications.append(notification)
        user['notifications'] = notifications
        conn.close()
        return user
    except:
        print("Database error. Failed to get user.")
        conn.close()
        return None

    


'''
Get All Users
Parameters:
    - None
Returns:
    - List of dictionaries with users' info
'''
def get_users():
    try:
        conn = mysql.connect()
        cursor = conn.cursor()
        users = []
        cursor.execute("SELECT *  FROM Users WHERE User_Deleted = False")
        found_users = cursor.fetchall()
        for row in found_users:
            user = {
                'user_id' : str(row[0]),
                'first_name' : str(row[1]),
                'last_name' : str(row[3]),
                'email' : str(row[5]),
                'role' : str(row[6])
            }
            users.append(user)
        conn.close()
        return users
    except:
        conn.close()
        return None
