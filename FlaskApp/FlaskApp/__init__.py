import sys

sys.path.append('/vagrant')

from flask import Flask, flash, render_template, json, request, jsonify, session, url_for, redirect
from flaskext.mysql import MySQL
#from flask_session import Session
from werkzeug import generate_password_hash, check_password_hash, secure_filename
from modules import rabbitmq
from FlaskApp.serverside.serverside_table import ServerSideTable
from FlaskApp.serverside import table_schemas
from datetime import timedelta
import datetime
import time
from time import strftime
from FlaskApp.FlaskApp import db
from FlaskApp.FlaskApp import log
from FlaskApp.FlaskApp import scanners
from FlaskApp.FlaskApp import users
from FlaskApp.FlaskApp import methods
import pytz
import json
import csv
import io
import pika

app = Flask(__name__)
#sess = Session()
mysql = MySQL()


'''session = {
    'logged_in': False,
    'user': None,
    'full_name': None,
    'view': None,
    'role': None
}'''


INFO = 'INFO'

# MySQL configurations
app.config['MYSQL_DATABASE_USER'] = 'master'
app.config['MYSQL_DATABASE_PASSWORD'] = 'NewAWSPassword1234'
app.config['MYSQL_DATABASE_DB'] = 'webscanner'
app.config['MYSQL_DATABASE_HOST'] = 'web-search.cmxgj4wqbqnn.us-west-2.rds.amazonaws.com'
mysql.init_app(app)

global SQL_Statement
SQL_Statement = "SELECT Results.Date_Found, Item.Item_Name, Results.Title, Results.URL, Results.Result_ID, Item.Type FROM Results INNER JOIN Item ON Results.Item_ID = Item.Item_ID"

# Session configurations
app.config['SESSION_TYPE'] = 'filesystem'


# Used for passing variables to all templates
@app.context_processor
def inject_session():
    return dict(session=session)


@app.route('/login', methods=['POST'])
def login():
    email = request.form['email']
    password = request.form['password']
    auth_result = users.authenticate(email, password)
    if auth_result['status'] == 'success':
        session['user'] = auth_result['id']
        session['full_name'] = auth_result['full_name']
        session['logged_in'] = True
        session['view'] = 'Standard'  # standard view by default
        session['role'] = auth_result['role']
        log.log(session['full_name'], 'Successful user login')
        return redirect(url_for('main'))
    else:
        log.log(None, 'Failed login attempt for ' + str(email))
        return render_template('login.html', message=auth_result['message'])


@app.route('/logout')
def logout():
    log.log(session['full_name'], 'User logout')
    #session['logged_in'] = False
    session.pop('user', None)
    session.pop('full_name', None)
    session.pop('logged_in', None)
    session.pop('view', None)
    session.pop('role', None)

    # session['user'], session['full_name'], session['logged_in'], session['view'], session['role'] = None
    return render_template('login.html', message=None)


@app.route("/")
def main():
    if (session.get('logged_in') != True):
        return render_template('login.html', message=None)
    else:
        session['view'] = 'Standard'
        conn = mysql.connect()
        cursor = conn.cursor()
        # get total scans ran
        cursor.execute("SELECT COUNT(*) from Scan_Event")
        result = cursor.fetchone()
        ran_scans = result[0]
        # get total results found
        cursor.execute("SELECT COUNT(*) from Results")
        result = cursor.fetchone()
        results = result[0]
        # get total scans scheduled
        cursor.execute("SELECT COUNT(*) FROM Scan WHERE Active = 1")
        result = cursor.fetchone()
        scheduled_scans = result[0]
        # get total search items
        cursor.execute("SELECT COUNT(*) from Item WHERE Deleted = False")
        result = cursor.fetchone()
        search_items = result[0]
        # get weekly result count
        week = methods.last_week()
        weekly_results = db.weekly_results(week)
        weekly_results[0]["Result_Count"] = cursor.execute("SELECT * FROM Results WHERE Date_Found >= (CURDATE() - INTERVAL 7 DAY) AND Date_Found < (curdate() - INTERVAL 6 DAY)")
        weekly_results[1]["Result_Count"] = cursor.execute("SELECT * FROM Results WHERE Date_Found >= (CURDATE() - INTERVAL 6 DAY) AND Date_Found < (curdate() - INTERVAL 5 DAY)")
        weekly_results[2]["Result_Count"] = cursor.execute("SELECT * FROM Results WHERE Date_Found >= (CURDATE() - INTERVAL 5 DAY) AND Date_Found < (curdate() - INTERVAL 4 DAY)")
        weekly_results[3]["Result_Count"] = cursor.execute("SELECT * FROM Results WHERE Date_Found >= (CURDATE() - INTERVAL 4 DAY) AND Date_Found < (curdate() - INTERVAL 3 DAY)")
        weekly_results[4]["Result_Count"] = cursor.execute("SELECT * FROM Results WHERE Date_Found >= (CURDATE() - INTERVAL 3 DAY) AND Date_Found < (curdate() - INTERVAL 2 DAY)")
        weekly_results[5]["Result_Count"] = cursor.execute("SELECT * FROM Results WHERE Date_Found >= (CURDATE() - INTERVAL 2 DAY) AND Date_Found < (curdate() - INTERVAL 1 DAY)")
        weekly_results[6]["Result_Count"] = cursor.execute("SELECT * FROM Results WHERE Date_Found >= (CURDATE() - INTERVAL 1 DAY) AND Date_Found < (curdate() - INTERVAL 0 DAY)")

        # get top five items found, use temp daylist until data for last week
        conn.close()
        today = datetime.date.today()
        delta = timedelta(days=1)
        day_list = [today-6*delta, today-5*delta, today-4*delta, today-3*delta, today-2*delta, today-delta, today]
        weekly_top, weekly_result_count = db.weekly_top_items(day_list)
        return render_template('index.html', ran_scans=ran_scans, results=results, scheduled_scans=scheduled_scans,
                               search_items=search_items, weekly_results=weekly_results, weekly_top=weekly_top,
                               weekly_result_count=weekly_result_count)

@app.route("/admin")
def admin():
    session['view'] = 'Admin'
    return render_template('admin.html', message=None)


@app.route('/scanSchedule')
def scanSchedule():
    conn = mysql.connect()
    cursor = conn.cursor()
    scan_schedules = []
    # get scan schedules, sort by date/time
    cursor.execute("SELECT * FROM Scan WHERE Active = 1 ORDER BY Next_Date ASC;")
    schedules = cursor.fetchall()
    for row in schedules:
        name = row[1]
        description = row[7]
        next = row[4]
        repeat = row[5]
        scan_id = row[0]
        if '1' in str(repeat):
            interval = 'Recurring'
        else:
            interval = 'One-Time'
        scan_info = name, description, next, interval, scan_id
        scan_schedules.append(scan_info)
    conn.close()
    return render_template('scan_schedule.html', schedules=scan_schedules)


@app.route('/scanHistory')
def scanHistory():
    conn = mysql.connect()
    cursor = conn.cursor()
    scan_events = []
    # get scan_events
    cursor.execute("SELECT * FROM Scan_Event ORDER BY Start_Time ASC;")
    events = cursor.fetchall()
    for row in events:
        event_id = row[0]
        scan_id = row[1]
        time = row[2]
        # get scan info for event
        cursor = conn.cursor()
        cursor.execute("SELECT * FROM Scan WHERE Scan_ID = " + str(scan_id))
        scans = cursor.fetchall()
        for scan in scans:
            scan_name = scan[1]
            scan_description = scan[7]
            repeat = scan[5]
            if '1' in str(repeat):
                scan_repeat = 'Recurring'
            else:
                scan_repeat = 'One-Time'
        # get results for event
        cursor = conn.cursor()
        cursor.execute("SELECT COUNT(*) from Results WHERE Event_ID = " + str(event_id))
        result = cursor.fetchone()
        event_results = result[0]
        scan_event = {
            "event_id": str(event_id),
            "scan_id": str(scan_id),
            "time": str(time),
            "name": str(scan_name),
            "description": str(scan_description),
            "frequency": str(scan_repeat),
            "result_count": str(event_results)
        }
        scan_events.append(scan_event)
    conn.close()
    return render_template('scan_history.html', scan_events=scan_events)


@app.route('/scanItems')
def scanItems():
    conn = mysql.connect()
    cursor = conn.cursor()
    items = []
    # get scan schedules, sort by date/time
    cursor.execute("SELECT *  FROM Item WHERE Deleted = False")
    found_items = cursor.fetchall()
    for row in found_items:
        item_ID = row[0]
        item_name = row[1]
        item_type = row[2]
        item_date_added = row[3]
        item_info = item_ID, item_name, item_type, item_date_added
        items.append(item_info)
    conn.close()
    return render_template('scan_items.html', items=items)


@app.route('/results_table', methods=['GET'])
def results_table():
    conn = mysql.connect()
    cursor = conn.cursor()
    scan_results = []
    # get scan schedules, sort by date/time
    global SQL_Statement
    cursor.execute(SQL_Statement)
    results = cursor.fetchall()
    if results:
        for row in results:
            result = {
                "A": str(row[0]),
                "B": str(row[1]),
                "C": str(row[2]),
                "D": str(row[3]),
                "E": str(row[4]),
                "F": str(row[4]),
                "G": str(row[5])
            }
            scan_results.append(result)
    conn.close()
    columns = table_schemas.SERVERSIDE_TABLE_COLUMNS
    data = ServerSideTable(request, scan_results, columns).output_result()
    return jsonify(data)


@app.route('/scanResults', methods=['POST', 'GET'])
def scanResults():
    global SQL_Statement
    # handle form load
    if request.method == 'GET':
        conn = mysql.connect()
        cursor = conn.cursor()
        SQL_Statement = "SELECT Results.Date_Found, Item.Item_Name, Results.Title, Results.URL, Results.Result_ID, Item.Type FROM Results INNER JOIN Item ON Results.Item_ID = Item.Item_ID"
        # get items for filters
        cursor.execute("SELECT * FROM Item")
        items = cursor.fetchall()
        return render_template('results.html', action="none", items=items)
    # handle filter load
    if request.method == 'POST':
        filters = request.form.getlist('filter_select')
        print(filters)
        items = []
        types = []
        date = []
        no_filter = True
        SQL_Statement = "SELECT Results.Date_Found, Item.Item_Name, Results.Title, Results.URL, Results.Result_ID, Item.Type FROM Results INNER JOIN Item ON Results.Item_ID = Item.Item_ID "
        for f in filters:
            if (f == 'item'):
                item_ids = request.form.getlist('search_item')
                # get each item ID
                for item_id in item_ids:
                    items.append(str(item_id))
                    print(str(item_id))
                first = True
                for item in items:
                    if first:
                        first = False
                        SQL_Statement += "WHERE Results.Item_ID = " + item + " "
                    else:
                        SQL_Statement += "OR Results.Item_ID = " + item + " "
                no_filter = False
                print(SQL_Statement)
            if (f == 'item_type'):
                item_types = request.form.getlist('item_type')
                for item_type in item_types:
                    types.append(str(item_type))
                first = True
                for item_type in types:
                    if first:
                        first = False
                        if no_filter:
                            SQL_Statement += "WHERE Item.Type LIKE \"" + item_type + "\" "
                        else:
                            SQL_Statement += "OR Item.Type LIKE \"" + item_type + "\" "
                    else:
                        SQL_Statement += "OR Item.Type LIKE \"" + item_type + "\" "
                no_filter = False
            if (f == 'date'):
                start = request.form.get('startDate')
                end = request.form.get('endDate')
                if no_filter:
                    SQL_Statement += "WHERE Results.Date_Found BETWEEN \'" + start + "\' and \'" + end + "\' "
                else:
                    SQL_Statement += "AND Results.Date_Found BETWEEN \'" + start + "\' and \'" + end + "\' "
        # get items for filters
        print(SQL_Statement)
        conn = mysql.connect()
        cursor = conn.cursor()
        cursor.execute("SELECT * FROM Item")
        items = cursor.fetchall()
        return render_template('results.html', action="none", items=items)


@app.route('/addItem', methods=['POST', 'GET'])
def addItem():
    # handle form load
    if request.method == 'GET':
        return render_template('add_item.html', action="show_add")
    # handle form submission
    if request.method == 'POST':
        request_action = request.form['action']
        err = []
        items = []
        action = None
        # add single item
        if request_action == 'add':
            action = "success"
            name = request.form['name']
            item_type = request.form.get('type')
            # get date added
            date_added = datetime.datetime.now()
            # insert in database
            item_ID = db.insert_item(name, item_type, date_added)
            if (item_ID == None):
                # display error message
                action = "fail"
                err.append("Error creating scan item.")
            else:
                # get aliases
                aliases = request.form.getlist('alias')
                for alias in aliases:
                    if alias:
                        db.insert_alias(alias, item_ID)
        # get multiple items from user upload
        if request_action == 'upload_items':
            action = 'uploaded_items'
            f = request.files['items_file']
            if not f:
                err = 'error'
            else:
                stream = io.StringIO(f.stream.read().decode("UTF8"), newline=None)
                csv_input = csv.reader(stream)
                for row in csv_input:
                    aliases = []
                    i = 2
                    while i < len(row):
                        if any(field.strip() for field in row):
                            aliases.append(row[i])
                        i += 1
                    aliases = filter(None, aliases)  # remove empty fields
                    item = {
                        'Name': row[0],
                        'Type': row[1],
                        'Aliases': aliases
                    }
                    items.append(item)
        # add multiple items, after user confirmation
        if request_action == 'add_uploaded':
            date_added = datetime.datetime.now()
            action = 'add_uploaded_complete'
            i = 1
            try:
                while request.form['name' + str(i)]:
                    item_name = request.form['name' + str(i)]
                    item_type = request.form['type' + str(i)]
                    item_ID = db.insert_item(item_name, item_type, date_added)
                    if (item_ID == None):  # check if item creation successful
                        err = 'fail'
                    else:  # get aliases
                        aliases = request.form.getlist('alias' + str(i))
                        print(aliases)
                        if aliases:
                            for alias in aliases:
                                db.insert_alias(alias, item_ID)
                    i += 1
            except:
                pass
        return render_template('add_item.html', action=action, err=err, items=items)


@app.route('/editItem', methods=['POST', 'GET'])
def editItem():
    # handle form load
    if request.method == 'GET':
        action = request.args['action']
        if action == 'edit':
            # get item info
            item_id = request.args['id']
            conn = mysql.connect()
            cursor = conn.cursor()
            cursor.execute("SELECT * FROM Item WHERE Item_ID = " + str(item_id))
            results = cursor.fetchall()
            for row in results:
                item_name = row[1]
                item_type = row[2]
            # get aliases for item
            cursor.execute("SELECT * FROM Item_Alias WHERE Alias_Deleted = False AND Item_ID = " + str(item_id))
            results = cursor.fetchall()
            aliases = []
            for row in results:
                alias_ID = row[0]
                alias_name = row[2]
                alias = alias_ID, alias_name
                aliases.append(alias)
            return render_template('edit_item.html', action="none", item_id=item_id, item_name=item_name,
                                   item_type=item_type, aliases=aliases)

        if action == 'delete':
            item_id = request.args['id']
            name = request.args['name']
            return render_template('delete_item.html', item_id=item_id, name=name, action="none")

        return render_template('edit_item.html', action="none")

    # handle form submission
    if request.method == 'POST':
        post_action = request.form['action']
        action = None
        message = None
        print('got post action: ' + str(post_action))
        if str(post_action) == 'deleteAlias':
            # delete alias
            alias_id = request.form['alias']
            if (db.deleteAlias(alias_id)):
                action = "success"
                message = "Alias deleted successfully!"
            else:
                action = "fail"
                message = "Failed to delete alias."
            return render_template('edit_item.html', action=action, message=message)
        elif str(post_action) == "updateItem":
            # update item
            item_id = request.form['item_id']
            item_name = request.form['name']
            item_type = request.form.get('type')
            if (db.updateItem(item_id, item_name, item_type)):
                # add new aliases
                aliases = request.form.getlist('alias')
                for alias in aliases:
                    if alias:
                        if (db.insert_alias(alias, item_id)):
                            action = "success"
                            message = "Item updated successfully!"
                        else:
                            action = "fail"
                            message = "Error creating new alias."
                            break
                # update existing aliases
                alias_ids = request.form.getlist('existing_id')
                ids = []
                for i in alias_ids:
                    ids.append(i)
                counter = 0
                existing_aliases = request.form.getlist('existing_alias')
                for alias in existing_aliases:
                    if (db.updateAlias(ids[counter], alias)):
                        action = "success"
                        message = "Item updated successfully!"
                        counter += 1
                    else:
                        action = "fail"
                        message = "Error creating new alias."
                        break
            else:
                action = "fail"
                message = "Failed to update item."
        elif str(post_action) == "confirmItemDelete":
            # delete item
            item_id = request.form['item_id']
            if (db.deleteItem(item_id)):
                action = "success"
                message = "Item deleted successfully!"
            else:
                action = "fail"
                message = "Error deleting item."
            return render_template('delete_item.html', action=action, message=message)

        return render_template('edit_item.html', action=action, message=message)


@app.route('/scheduleScan', methods=['POST', 'GET'])
def scheduleScan():
    # handle form load
    if request.method == 'GET':
        conn = mysql.connect()
        cursor = conn.cursor()
        # get and set return variables methods
        cursor.execute("SELECT * FROM Scanner WHERE Deleted = False")
        scan_methods = cursor.fetchall()
        cursor.execute("SELECT * FROM Users")
        users = cursor.fetchall()
        cursor.execute("SELECT * FROM Item")
        items = cursor.fetchall()
        # get notification methods
        cursor.execute("SELECT * FROM Notification_Methods")
        notifications = cursor.fetchall()
        scan_notifications = []
        scan = []
        scan_items = []
        scanners = []
        scan_id = None
        scan_name = None
        action = request.args['action']
        if action == 'delete':
            scan_id = request.args['id']
            scan_name = request.args['name']
        elif action == 'schedule_scan' or action == None:
            action = 'schedule'
        elif action == 'edit':
            scan_id = request.args['id']
            # get scan info
            print("SELECT * FROM Scan WHERE Scan_ID = " + str(scan_id))
            cursor.execute("SELECT * FROM Scan WHERE Scan_ID = " + str(scan_id))
            results = cursor.fetchall()
            for row in results:
                if '1' in str(row[5]):
                    repeat = '1'
                else:
                    repeat = '0'
                if '1' in str(row[8]):
                    all_products = '1'
                else:
                    all_products = '0'
                if row[3] == None:
                    end_date = '0000-00-00 00:00:00'
                else:
                    end_date = datetime.datetime.strptime(str(row[3]), '%Y-%m-%d %H:%M:%S')
                scan = {
                    "name": str(row[1]),
                    "scan_id": str(scan_id),
                    "start_date": datetime.datetime.strptime(str(row[2]), '%Y-%m-%d %H:%M:%S'),
                    "end_date": end_date,
                    "repeat": str(repeat),
                    "description": str(row[7]),
                    "all_products": str(all_products),
                    "gap_days": str(row[9]),
                    "gap_time": str(row[10]),
                    "gap_weeks": str(row[11]),
                    "gap_months": str(row[12])
                }
            # get custom products if necessary
            if scan["all_products"] != '1':
                cursor.execute("SELECT * FROM Scan_Items WHERE Scan_ID = " + str(scan_id))
                results = cursor.fetchall()
                for row in results:
                    item = {
                        "item_id": str(row[2])
                    }
                    scan_items.append(item)
                    print(item)
            # get scanner methods
            cursor.execute("SELECT * FROM Scan_Scanner WHERE Scan_ID = " + str(scan_id))
            results = cursor.fetchall()
            for row in results:
                scanner = {
                    "scanner_id": str(row[2])
                }
                scanners.append(scanner)
                print(scanner)
            # get notifications
            cursor.execute("SELECT * FROM Scan_Notification WHERE Scan_ID = " + str(scan_id))
            results = cursor.fetchall()
            for row in results:
                notification = {
                    "notification_id": str(row[2])
                }
                scan_notifications.append(notification)
                print(notification)
        conn.close()
        return render_template('schedule_scan.html', action=action, scan=scan, scanners=scanners,
                               scan_notifications=scan_notifications, scan_items=scan_items, scan_methods=scan_methods,
                               users=users, items=items, notifications=notifications, scan_id=scan_id,
                               scan_name=scan_name)
    # handle form submission
    if request.method == 'POST':
        action = request.form['action']
        scan_name = ''
        if action == 'confirm_delete':  # delete scan
            scan_name = request.form['name']
            scan_id = request.form['id']
            if db.terminate_scan(str(scan_id)):
                err = 'none'
            else:
                err = 'error'
            action = 'deleted'
        if action == 'edit_scan':  # edit scan
            scan_id = request.form['id']
            scan_info = methods.get_scan_info(request)
            err = db.update_scan(scan_info, scan_id)
            if err:
                err = 'none'
            else:
                err = 'error'
            action = 'updated'
        else:  # schedule new scan
            scan_info = methods.get_scan_info(request)
            err = db.schedule_scan(scan_info)
            if err:
                action = 'success'
            else:
                action = 'fail'
        ##Notify the control engine of a change in schedule
        print("GOING TO PUSH TO RABBIT MQ NOW")
        rabbitmq.push_to_queue('site', '{"nothing":"nothing"}')
        return render_template('schedule_scan.html', action=action, err=err, scan_name=scan_name)


###### USER MANAGEMENT ROUTES ######

@app.route('/users')
def Users():
    all_users = users.get_users()
    return render_template('users.html', users=all_users)


@app.route('/editUser', methods=['POST', 'GET'])
def editUser():
    # handle get requests
    if request.method == 'GET':
        action = request.args['action']
        if action == 'edit':
            user_id = request.args['id']
            edit_user = users.get_user(user_id)
            return render_template('edit_user.html', user=edit_user, action='edit')
        if action == 'delete':
            user_id = request.args['id']
            full_name = str(request.args['firstname']) + str(request.args['lastname'])
            return render_template('edit_user.html', action='delete', name=full_name, user_id=user_id)
        if action == 'add':
            return render_template('edit_user.html', action='add')
        return render_template('users.html', action=None)

    # handle form submission
    if request.method == 'POST':
        post_action = request.form['action']

        # update user
        if str(post_action) == "updateUser":
            # get new notifications
            n_notifications = []
            n_values = request.form.getlist('value')
            n_types = request.form.getlist('type')
            if n_types:
                for n_value, n_type in zip(n_values, n_types):
                    if n_value is not '':
                        notification = {'n_type': n_type, 'n_value': n_value}
                        n_notifications.append(notification)
            # get existing notifications
            e_notifications = []
            n_values = request.form.getlist('existing_nvalue')
            n_types = request.form.getlist('existing_ntype')
            n_ids = request.form.getlist('existing_id')
            if n_types:
                for n_value, n_type, n_id in zip(n_values, n_types, n_ids):
                    notification = {'n_id': n_id, 'n_type': n_type, 'n_value': n_value}
                    e_notifications.append(notification)
            # update db
            user = {
                'user_id': str(request.form['user_id']),
                'first_name': str(request.form['first_name']),
                'last_name': str(request.form['last_name']),
                'email': str(request.form['email']),
                'role': str(request.form.get('role')),
                'new_notifications': n_notifications,
                'existing_notifications': e_notifications
            }
            if users.update_user(user):
                message = 'User updated successfully!'
            else:
                message = 'Failed to update user.'
            return render_template('edit_user.html', action='complete', message=message)

        # delete user - post-confirmation
        if str(post_action) == "deleteUser":
            user_id = request.form['user_id']
            if users.delete_user(user_id):
                message = "User deleted successfully!"
            else:
                message = "Error deleting user."
            return render_template('edit_user.html', action='complete', message=message)

        # add user
        if str(post_action) == 'addUser':
            # get notifications
            notifications = []
            n_values = request.form.getlist('value')
            n_types = request.form.getlist('type')
            if n_types:
                for n_value, n_type in zip(n_values, n_types):
                    if n_value is not '':
                        notification = {'n_type': n_type, 'n_value': n_value}
                        notifications.append(notification)
            user = {
                'first_name': str(request.form['first_name']),
                'last_name': str(request.form['last_name']),
                'email': str(request.form['email']),
                'password': str(request.form['password']),
                'role': str(request.form.get('role')),
                'notifications': notifications
            }
            if users.create_user(user):
                message = "User created successfully!"
            else:
                message = "Error deleting user."
            return render_template('edit_user.html', action='complete', message=message)

        #return render_template('edit_user.html', action=action, message=message)


###### SCANNER MANAGEMENT ROUTES ######
@app.route('/scanners')
def Scanners():
    all_scanners = scanners.get_scanners()
    return render_template('scanners.html', scanners=all_scanners)


@app.route('/editScanner', methods=['POST', 'GET'])
def editScanner():
    # handle get requests
    if request.method == 'GET':
        action = request.args['action']
        if action == 'edit':
            scanner_id = request.args['id']
            scanner = scanners.get_scanner(scanner_id)
            return render_template('edit_scanner.html', scanner=scanner, action='edit')
        if action == 'delete':
            scanner_id = request.args['id']
            name = request.args['name']
            return render_template('edit_scanner.html', action='delete', name=name, scanner_id=scanner_id)
        if action == 'add':
            return render_template('edit_scanner.html', action='add')
        return render_template('scanners.html', action=None)

    # handle form submission
    if request.method == 'POST':
        action = request.form['action']

        # update scanner
        if action == 'updateScanner':
            scanner = {
                'scanner_id': str(request.form['scanner_id']),
                'name': str(request.form['name']),
                'description': str(request.form['description']),
                'path': str(request.form['path']),
                'working_directory': str(request.form['working_directory'])
            }
            if scanners.update_scanner(scanner):
                message = "Scanner updated successfully!"
            else:
                message = "Error updating scanner."
            return render_template('edit_scanner.html', action='complete', message=message)

        # create scanner
        if action == 'addScanner':
            scanner = {
                'name': str(request.form['name']),
                'description': str(request.form['description']),
                'path': str(request.form['path']),
                'working_directory': str(request.form['working_directory'])
            }
            if scanners.create_scanner(scanner):
                message = "Scanner created successfully!"
            else:
                message = "Error creating scanner."
            return render_template('edit_scanner.html', action='complete', message=message)

        # delete scanner
        if action == 'deleteScanner':
            if scanners.delete_scanner(str(request.form['scanner_id'])):
                message = "Scanner deleted successfully!"
            else:
                message = "Error deleting scanner."
            return render_template('edit_scanner.html', action='complete', message=message)


###### LOG ROUTES ######
@app.route('/logs')
def logs():
    u_logs = log.get_logs(['user.log'])
    s_logs = log.get_logs(['../../control/info.log', '../../control/errors.log'])
    return render_template('logs.html', u_logs=u_logs, s_logs=s_logs)


###### SYSTEM HEALTH ROUTES ######
@app.route('/health')
def health():
    health = log.system_health()
    return render_template('health.html', s_health=health)


###### UPDATE / MAIN ROUTES ######

@app.route('/update', methods=['POST', 'GET'])
def update():
    from os import system
    print("test")
    test = system("cd /vagrant && git pull")
    return test


if __name__ == "__main__":
    app.secret_key = 'super secret key'
    app.config['SESSION_TYPE'] = 'filesystem'

    #sess.init_app(app)

    app.debug = True
    app.run(host='0.0.0.0')