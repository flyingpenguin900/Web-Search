from flask import Flask, render_template, json, request, jsonify
from flaskext.mysql import MySQL
from werkzeug import generate_password_hash, check_password_hash
import datetime
import time


app = Flask(__name__)

mysql = MySQL()

# MySQL configurations
app.config['MYSQL_DATABASE_USER'] = 'master'
app.config['MYSQL_DATABASE_PASSWORD'] = 'NewAWSPassword1234'
app.config['MYSQL_DATABASE_DB'] = 'webscanner'
app.config['MYSQL_DATABASE_HOST'] = 'web-search.cmxgj4wqbqnn.us-west-2.rds.amazonaws.com'
mysql.init_app(app)


##### Database Functions ####

# Function for creating a scan
# Returns the new scan's ID or None
def insert_scan(name, description, start_date, end_date, repeat, all_products, gap_days, gap_time, gap_weeks, gap_months):
    conn = mysql.connect()
    cursor = conn.cursor()
    command = "INSERT INTO Scan (Scan_Name, Start_Date, End_Date, Next_Date, Scan_Repeat, Active, Description, All_Products, Gap_Days, Gap_Time, Gap_Weeks, Gap_Months) VALUES ('%s', '%s', '%s', '%s', %s, %s, '%s', %s, '%s', '%s', '%s', '%s')" % \
        (str(name), str(start_date), str(end_date), str(start_date), repeat, 1, str(description), all_products, str(gap_days), str(gap_time), str(gap_weeks), str(gap_months))
    try: # try inserting in database
        cursor.execute(command)
        conn.commit()
        new_id = cursor.lastrowid
        print("---> Successfully created scan. Scan_ID: " + str(new_id))
        conn.close()
        return new_id
    except:
        conn.rollback()
        print("---> Failed to create scan")
        conn.close()
        return None

# Function for updating a scheduled scan
# Returns true if update successful or false if 0 rows returned
def updateScan(scan_id, name, description, start_date, end_date, repeat, all_products, gap_days, gap_time, gap_weeks, gap_months):
    conn = mysql.connect()
    cursor = conn.cursor()
    affected = False
    try:
        count = cursor.execute("UPDATE Scan SET Scan_Name = %s, Description = %s, Start_Date = %s, End_Date = %s, All_Products = %s, Scan_Repeat = %s, Gap_Days = %s, Gap_Time = %s, Gap_Months = %s, Gap_Weeks = %s WHERE Scan_ID = %s ", 
            (str(name), str(description), str(start_date), str(end_date), str(all_products), str(repeat), str(gap_days), str(gap_time), str(gap_months), + str(scan_id))) 
        conn.commit()
        affected = True
        print("---> Successfully updated Scan_ID: " + str(scan_id))
    except:
        conn.rollback()
        print("---> Failed to update Scan_ID: " + str(scan_id))
    conn.close()
    return affected


# Function for terminating a scan
# Returns True if termination successful
def terminate_scan(scan_id):
    conn = mysql.connect()
    cursor = conn.cursor()
    affected = False
    try:
        count = cursor.execute("UPDATE Scan SET Active = False WHERE Scan_ID = " + str(scan_id))
        conn.commit()
        affected = True
        print("---> Successfully terminated scan: " + str(scan_id))
    except:
        conn.rollback()
        print("---> Failed to terminate scan: " + str(scan_id))
    conn.close()
    return affected


# Function for creating a scheduled scan method
# Returns the new scheduled_scan_method's ID or None
def insert_scan_scanner(scanner_id, scan_id):
    conn = mysql.connect()
    cursor = conn.cursor()
    command = "INSERT INTO Scan_Scanner (Scanner_ID, Scan_ID) VALUES ('%s', '%s')" % \
        (str(scanner_id), str(scan_id))
    try: # try inserting in database
        cursor.execute(command)
        conn.commit()
        new_id = cursor.lastrowid
        print("---> Successfully created scan scanner. Scan_Scanner_ID: " + str(new_id))
        conn.close()
        return new_id
    except:
        conn.rollback()
        print("---> Failed to create scheduled scan scanner for scanner ID: " + str(scanner_id))
        conn.close()  
        return None  


# Function for creating a notification_method AND scan_notification
# Returns the new scheduled_scan_notification's ID or None
def insert_scan_notification(scan_id, n_method):
    conn = mysql.connect()
    cursor = conn.cursor()
    command = "INSERT INTO Scan_Notification (Scan_ID, Method_ID) VALUES ('%s', '%s')" % \
        (str(scan_id), str(n_method))
    # insert scan_notification
    try: 
        cursor.execute(command)
        conn.commit()
        new_id = cursor.lastrowid
        print("---> Successfully created scan notification. Scan_Notification_ID: " + str(new_id))
        conn.close()
        return new_id
    except:
        conn.rollback()
        print("---> Failed to create scan notification!")
        conn.close()
        return None



# Function for creating a scan item
# * Use this method if user selected "Custom" for scan_items *
# Pass:
#     -> item_ID of selected item for scan
#     -> scan_ID 
# Returns the new scan_item's ID or None
def insert_scan_item(scan_id, item_id):
    conn = mysql.connect()
    cursor = conn.cursor()
    command = "INSERT INTO Scan_Items (Scan_ID, Item_ID) VALUES ('%s', '%s')" % \
        (str(scan_id), str(item_id))
    try: # try inserting in database
        cursor.execute(command)
        conn.commit()
        new_id = cursor.lastrowid
        print("---> Successfully created scan item. Scan_Items_ID: " + str(new_id))
        conn.close()
        return new_id
    except:
        conn.rollback()
        print("---> Failed to create scheduled scan item!")
        conn.close() 
        return None


# Function for creating a new item
# Pass:
#     -> item name
#     -> item type
#     -> date added
# Returns the new item's ID or None
def insert_item(item_name, item_type, date_added):
    conn = mysql.connect()
    cursor = conn.cursor()
    command = "INSERT INTO Item (Item_Name, Type, Date_Added) VALUES ('%s', '%s', '%s')" % \
        (str(item_name), str(item_type), str(date_added))
    try: # try inserting in database
        cursor.execute(command)
        conn.commit()
        new_id = cursor.lastrowid
        print("---> Successfully created new item. Item_ID: " + str(new_id))
        conn.close()
        return new_id
    except:
        conn.rollback()
        print("---> Failed to create item!")
        conn.close() 
        return None


# Function for creating a new alias
# Pass:
#     -> alias name
#     -> item_ID
# Returns the new alias ID or None
def insert_alias(alias_name, item_ID):
    conn = mysql.connect()
    cursor = conn.cursor()
    command = "INSERT INTO Item_Alias (Item_ID, Alias) VALUES ('%s', '%s')" % \
        (str(item_ID), str(alias_name))
    try: # try inserting in database
        cursor.execute(command)
        conn.commit()
        new_id = cursor.lastrowid
        print("---> Successfully created alias. Alias_ID: " + str(new_id))
        conn.close()
        return new_id
    except:
        conn.rollback()
        print("---> Failed to create alias!")
        conn.close() 
        return None



# Function for deleting an alias
# Returns true if update successful or false if 0 rows returned
def deleteAlias(alias_id):
    conn = mysql.connect()
    cursor = conn.cursor()
    affected = False
    try:
        count = cursor.execute("UPDATE Item_Alias SET Alias_Deleted = True WHERE Alias_ID = " + str(alias_id))
        conn.commit()
        affected = True
        print("---> Successfully deleted alias_id: " + str(alias_id))
    except:
        conn.rollback()
        print("---> Failed to delete alias_id: " + str(alias_id))
    conn.close()
    return affected


# Function for updating an alias
# Returns true if update successful or false if 0 rows returned
def updateAlias(alias_id, alias):
    conn = mysql.connect()
    cursor = conn.cursor()
    affected = False
    try:
        count = cursor.execute("UPDATE Item_Alias SET Alias = \"" + str(alias) + "\" WHERE Alias_ID = " + str(alias_id))
        conn.commit()
        affected = True
        print("---> Successfully updated Alias_ID: " + str(alias_id))
    except:
        conn.rollback()
        print("---> Failed to update Alias_ID: " + str(alias_id))
    conn.close()
    return affected


# Function for updating an item
# Returns true if update successful or false if 0 rows returned
def updateItem(item_id, item_name, item_type):
    conn = mysql.connect()
    cursor = conn.cursor()
    affected = False
    try:
        count = cursor.execute("UPDATE Item SET Item_Name = \"" + str(item_name) + "\", Type = \"" + str(item_type) + "\" WHERE Item_ID = " + str(item_id))
        conn.commit()
        affected = True
        print("---> Successfully updated Item_id: " + str(item_id))
    except:
        conn.rollback()
        print("---> Failed to update Item_ID: " + str(item_id))
    conn.close()
    return affected


# Function for deleting an item
# Returns true if update successful or false if 0 rows returned
def deleteItem(item_id):
    conn = mysql.connect()
    cursor = conn.cursor()
    affected = False
    try:
        count = cursor.execute("UPDATE Item SET Deleted = True WHERE Item_ID = " + str(item_id))
        conn.commit()
        affected = True
        print("---> Successfully deleted Item_ID: " + str(item_id))
    except:
        conn.rollback()
        print("---> Failed to delete Item_ID: " + str(item_id))
    conn.close()
    return affected


# Function for getting all Scanners for a Scan
# returns list containing dictionary item for each scanner
def get_scan_scanners(scan_id):
    conn = mysql.connect()
    cursor = conn.cursor()
    scanners = []
    try:
        cursor.execute("SELECT * FROM Scan_Scanner WHERE Scan_ID = " + str(scan_id))
        results = cursor.fetchall()
        for row in results:
            scanner = {
                "Scan_Scanner_ID": str(row[0]),
                "Scan_ID": str(row[1]),
                "Scanner_ID": str(row[2])
            }
            scanners.append(scanner)
    except:
        conn.rollback()
        print("---> Failed to retrieve scanners for scan ID: " + str(scan_id))
    conn.close()
    return scanners



# NEW METHOD - pass JSON object with Scan info
def schedule_scan(scan_info):
    scan_ID = insert_scan(
        scan_info['Name'], 
        scan_info['Description'], 
        scan_info['Start'], 
        scan_info['End'], 
        scan_info['Repeat'], 
        scan_info['All_Products'], 
        scan_info['Gap_Days'], 
        scan_info['Gap_Time'], 
        scan_info['Gap_Weeks'], 
        scan_info['Gap_Months']
    )
    if scan_ID is None:
        return "Error creating scan."
    if scan_info['Methods']:
        for i in scan_info['Methods']:
            if insert_scan_scanner(i, scan_ID) is None:
                return "Error associating scanner. Scanner ID: " + str(i) + " New Scan ID: " + str(scan_ID) + " Aborted. "    
    if scan_info['Items']:
        for i in scan_info['Items']:
            if insert_scan_item(scan_ID, i) is None:
                return "Error associating search item. Item ID: " + str(i) + " New Scan ID: " + str(scan_ID) + " Aborted. "
    if scan_info['Notifications']:
        for i in scan_info['Notifications']:
            if insert_scan_notification(scan_ID, i) is None:
                return "Error associating search item. Item ID: " + str(i) + " New Scan ID: " + str(scan_ID) + " Aborted. "
    return True


# NEW METHOD - pass JSON object with Scan info
def update_scan(scan_info, scan_id):
    updated = updateScan(
        scan_id,
        scan_info['Name'], 
        scan_info['Description'], 
        scan_info['Start'], 
        scan_info['End'], 
        scan_info['Repeat'], 
        scan_info['All_Products'], 
        scan_info['Gap_Days'], 
        scan_info['Gap_Time'], 
        scan_info['Gap_Weeks'], 
        scan_info['Gap_Months']
    )
    if not updated:
        return "Error updating scan."

    # TODO: update methods, items, notifications for scan

    return True



# Get a list of result totals found for the last 7 days
# Pass list with the last 7 dates (datetimes)
# Returns dictionary of days/totals
def weekly_results(day_list):
    results = []
    conn = mysql.connect()
    cursor = conn.cursor()
    for day in day_list:
        count = cursor.execute("SELECT * FROM Results WHERE Results.Date_Found BETWEEN \'" + str(day) + "\' and \'" + str(day) + " 23:59:59\'")
        conn.commit()
        results.append({
            "Date": str(day.strftime("%m/%d")),
            "Result_Count":count
        })
    return results


# Get 5 search items with the most results found for specified day_list
# Pass list of days (typically last week so 7 days)
# Returns list of top five items/result count and total_results for day_list
def weekly_top_items(day_list):
    conn = mysql.connect()
    cursor = conn.cursor()
    # get total results for day_list
    cursor.execute("SELECT COUNT(*) FROM Results WHERE Results.Date_Found BETWEEN \'" + str(day_list[0]) + "\' and \'" + str(day_list[-1]) + " 23:59:59\'")
    total_results = cursor.fetchone()
    conn.commit()
    # get list of all items to check totals
    cursor.execute("SELECT * FROM Item")
    items = cursor.fetchall()
    # get total results for each item
    results = []
    for item in items:
        cursor.execute("SELECT COUNT(*) FROM Results WHERE Results.Date_Found BETWEEN \'" + str(day_list[0]) + "\' and \'" + str(day_list[-1]) + " 23:59:59\' AND Results.Item_ID = " + str(item[0]))
        result_count = cursor.fetchone()
        if total_results[0] == 0: 
            percent = 50
        else:
            percent = "{0:.0f}".format((result_count[0]/total_results[0]) * 100)     
        results.append({"Name": str(item[1]),"Result_Count":result_count[0],"Percent":percent}) 
    # sort list by result_count
    sorted_list = list(reversed(sorted(results, key=lambda k: k['Result_Count'])))
    top_five = sorted_list[:5] 
    return top_five, total_results[0]




