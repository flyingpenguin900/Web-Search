# Database can be accessed naturally in python.

## Here is an example.
```python
import database.orm as db

all_scans = db.session.query(db.Scan).all()

#alternativly
scans = db.session.query(db.Scan)
first_scan = scans.first()

#alternativly, you can filter the results
active_scans = db.session.query(db.Scan).filter_by(active=1)

#say you want some information on the scanners on a scan
first_scan.Scanners[0].Path

#say you want a User, and all their notification methods
db.session.query(db.Users).filter_by(User_ID=1).one().Notification_Methods[0].Notification_Type
```

TODO: More work on this readme.