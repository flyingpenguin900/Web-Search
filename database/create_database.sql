ALTER TABLE `Item_Alias` DROP FOREIGN KEY `fk_Item_Alias`;
ALTER TABLE `Scan_Event` DROP FOREIGN KEY `fk_Scan_Event_Scan_1`;
ALTER TABLE `Scan_Scanner` DROP FOREIGN KEY `fk_Scan_Scanner_Scan_1`;
ALTER TABLE `Scan_Scanner` DROP FOREIGN KEY `fk_Scan_Scanner_Scanners_1`;
ALTER TABLE `Results` DROP FOREIGN KEY `fk_Results_Item_1`;
ALTER TABLE `Results` DROP FOREIGN KEY `fk_Results_Scan_1`;
ALTER TABLE `Results` DROP FOREIGN KEY `fk_Results_Scan_Event_1`;
ALTER TABLE `Results` DROP FOREIGN KEY `fk_Results_Scanners_1`;
ALTER TABLE `Notification_Methods` DROP FOREIGN KEY `fk_Notification_Methods_Users_1`;
ALTER TABLE `Results` DROP FOREIGN KEY `fk_Results_Item_Alias_1`;
ALTER TABLE `Scan_Notification` DROP FOREIGN KEY `fk_Scan_Notification_Scan_1`;
ALTER TABLE `Scan_Notification` DROP FOREIGN KEY `fk_Scan_Notification_Notification_Methods_1`;
ALTER TABLE `Scanner_Options` DROP FOREIGN KEY `fk_Scanner_Op_Scanner_1`;
ALTER TABLE `Scan_Scanner_Options` DROP FOREIGN KEY `fk_Scan_Scanner_Options_Scan_Scanner_1`;
ALTER TABLE `Scan_Scanner_Options` DROP FOREIGN KEY `fk_Scan_Scanner_Options_Scanner_Op_1`;
ALTER TABLE `Scan_Items` DROP FOREIGN KEY `fk_Scan_Items_Scan_1`;
ALTER TABLE `Scan_Items` DROP FOREIGN KEY `fk_Scan_Items_Item_1`;

DROP TABLE `Users`;
DROP TABLE `Scanner`;
DROP TABLE `Scan`;
DROP TABLE `Item`;
DROP TABLE `Scan_Event`;
DROP TABLE `Results`;
DROP TABLE `Item_Alias`;
DROP TABLE `Scan_Scanner`;
DROP TABLE `Notification_Methods`;
DROP TABLE `Scan_Notification`;
DROP TABLE `Scanner_Options`;
DROP TABLE `Scan_Scanner_Options`;
DROP TABLE `Scan_Items`;

CREATE TABLE `Users` (
`User_ID` int(11) NOT NULL AUTO_INCREMENT,
`First_Name` varchar(255) NOT NULL,
`Middle_Name` varchar(255) NULL,
`Last_Name` varchar(255) NOT NULL,
`Password` varchar(255) NOT NULL,
PRIMARY KEY (`User_ID`) 
);

CREATE TABLE `Scanner` (
`Scanner_ID` int(11) NOT NULL AUTO_INCREMENT,
`Name` varchar(255) NOT NULL,
`Description` varchar(255) NULL,
`Path` varchar(255) NOT NULL,
`Date_Added` datetime NOT NULL,
`Deleted` bit(1) NOT NULL DEFAULT 0,
PRIMARY KEY (`Scanner_ID`) 
);

CREATE TABLE `Scan` (
`Scan_ID` int(11) NOT NULL AUTO_INCREMENT,
`Scan_Name` varchar(255) NOT NULL,
`Start_Date` datetime NOT NULL,
`End_Date` datetime NULL,
`Next_Date` datetime NULL,
`Scan_Repeat` bit(1) NOT NULL,
`Active` bit(1) NOT NULL DEFAULT 1,
`Description` varchar(255) NULL,
`All_Products` bit(1) NULL,
`Gap_Days` int(255) NULL DEFAULT 0,
`Gap_Time` time NOT NULL DEFAULT 00:00:00,
`Gap_Weeks` int(255) NULL DEFAULT 0,
`Gap_Months` int(255) NULL DEFAULT 0,
PRIMARY KEY (`Scan_ID`) 
);

CREATE TABLE `Item` (
`Item_ID` int(11) NOT NULL AUTO_INCREMENT,
`Name` varchar(255) NULL,
`Type` enum('Product','Keyword') NULL,
`Date_Added` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
`Deleted` bit(1) NOT NULL DEFAULT 0,
PRIMARY KEY (`Item_ID`) 
);

CREATE TABLE `Scan_Event` (
`Event_ID` int(11) NOT NULL AUTO_INCREMENT,
`Scan_ID` int(11) NOT NULL,
`Start_Time` datetime NOT NULL,
`End_Time` datetime NULL,
PRIMARY KEY (`Event_ID`) 
);

CREATE TABLE `Results` (
`Result_ID` int(11) NOT NULL AUTO_INCREMENT,
`Item_ID` int(11) NOT NULL,
`Alias_ID` int(11) NULL,
`Scan_ID` int(11) NOT NULL,
`Event_ID` int(11) NOT NULL,
`Scanner_ID` int(11) NOT NULL,
`URL` varchar(255) NULL,
`Title` varchar(255) NULL,
`Date_Posted` datetime NULL,
`Data` text NULL,
`Date_Found` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
PRIMARY KEY (`Result_ID`) 
);

CREATE TABLE `Item_Alias` (
`Alias_ID` int(11) NOT NULL AUTO_INCREMENT,
`Item_ID` int(11) NOT NULL,
`Alias` varchar(255) NOT NULL,
`Alias_Deleted` bit(1) NOT NULL DEFAULT 0,
PRIMARY KEY (`Alias_ID`) 
);

CREATE TABLE `Scan_Scanner` (
`Scan_Scanner_ID` int(11) NOT NULL AUTO_INCREMENT,
`Scan_ID` int(11) NOT NULL,
`Scanner_ID` int(11) NOT NULL,
PRIMARY KEY (`Scan_Scanner_ID`) 
);

CREATE TABLE `Notification_Methods` (
`Method_ID` int(11) NOT NULL AUTO_INCREMENT,
`User_ID` int(11) NOT NULL,
`Notification_Type` enum('phone','e-mail','text') NOT NULL,
`Notification_Value` varchar(255) NOT NULL,
PRIMARY KEY (`Method_ID`) 
);

CREATE TABLE `Scan_Notification` (
`Scan_Notification_ID` int(11) NOT NULL AUTO_INCREMENT,
`Scan_ID` int(11) NOT NULL,
`Method_ID` int(11) NOT NULL,
PRIMARY KEY (`Scan_Notification_ID`) 
);

CREATE TABLE `Scanner_Options` (
`Option_ID` int(11) NOT NULL AUTO_INCREMENT,
`Scanner_ID` int(11) NOT NULL,
`Type` enum('bool','text','list') NOT NULL,
`Name` varchar(255) NOT NULL,
`Default` varchar(255) NULL,
PRIMARY KEY (`Option_ID`) 
);

CREATE TABLE `Scan_Scanner_Options` (
`Scan_Scan_Op_ID` int(11) NOT NULL AUTO_INCREMENT,
`Scan_Scanner_ID` int(11) NOT NULL,
`Option_ID` int(11) NOT NULL,
`Value` varchar(255) NOT NULL,
PRIMARY KEY (`Scan_Scan_Op_ID`) 
);

CREATE TABLE `Scan_Items` (
`Scan_Items_ID` int(11) NOT NULL AUTO_INCREMENT,
`Scan_ID` int(11) NOT NULL,
`Item_ID` int(11) NOT NULL,
PRIMARY KEY (`Scan_Items_ID`) 
);


ALTER TABLE `Item_Alias` ADD CONSTRAINT `fk_Item_Alias` FOREIGN KEY (`Item_ID`) REFERENCES `Item` (`Item_ID`);
ALTER TABLE `Scan_Event` ADD CONSTRAINT `fk_Scan_Event_Scan_1` FOREIGN KEY (`Scan_ID`) REFERENCES `Scan` (`Scan_ID`);
ALTER TABLE `Scan_Scanner` ADD CONSTRAINT `fk_Scan_Scanner_Scan_1` FOREIGN KEY (`Scan_ID`) REFERENCES `Scan` (`Scan_ID`);
ALTER TABLE `Scan_Scanner` ADD CONSTRAINT `fk_Scan_Scanner_Scanners_1` FOREIGN KEY (`Scanner_ID`) REFERENCES `Scanner` (`Scanner_ID`);
ALTER TABLE `Results` ADD CONSTRAINT `fk_Results_Item_1` FOREIGN KEY (`Item_ID`) REFERENCES `Item` (`Item_ID`);
ALTER TABLE `Results` ADD CONSTRAINT `fk_Results_Scan_1` FOREIGN KEY (`Scan_ID`) REFERENCES `Scan` (`Scan_ID`);
ALTER TABLE `Results` ADD CONSTRAINT `fk_Results_Scan_Event_1` FOREIGN KEY (`Event_ID`) REFERENCES `Scan_Event` (`Event_ID`);
ALTER TABLE `Results` ADD CONSTRAINT `fk_Results_Scanners_1` FOREIGN KEY (`Scanner_ID`) REFERENCES `Scanner` (`Scanner_ID`);
ALTER TABLE `Notification_Methods` ADD CONSTRAINT `fk_Notification_Methods_Users_1` FOREIGN KEY (`User_ID`) REFERENCES `Users` (`User_ID`);
ALTER TABLE `Results` ADD CONSTRAINT `fk_Results_Item_Alias_1` FOREIGN KEY (`Alias_ID`) REFERENCES `Item_Alias` (`Alias_ID`);
ALTER TABLE `Scan_Notification` ADD CONSTRAINT `fk_Scan_Notification_Scan_1` FOREIGN KEY (`Scan_ID`) REFERENCES `Scan` (`Scan_ID`);
ALTER TABLE `Scan_Notification` ADD CONSTRAINT `fk_Scan_Notification_Notification_Methods_1` FOREIGN KEY (`Method_ID`) REFERENCES `Notification_Methods` (`Method_ID`);
ALTER TABLE `Scanner_Options` ADD CONSTRAINT `fk_Scanner_Op_Scanner_1` FOREIGN KEY (`Scanner_ID`) REFERENCES `Scanner` (`Scanner_ID`);
ALTER TABLE `Scan_Scanner_Options` ADD CONSTRAINT `fk_Scan_Scanner_Options_Scan_Scanner_1` FOREIGN KEY (`Scan_Scanner_ID`) REFERENCES `Scan_Scanner` (`Scan_Scanner_ID`);
ALTER TABLE `Scan_Scanner_Options` ADD CONSTRAINT `fk_Scan_Scanner_Options_Scanner_Op_1` FOREIGN KEY (`Option_ID`) REFERENCES `Scanner_Options` (`Option_ID`);
ALTER TABLE `Scan_Items` ADD CONSTRAINT `fk_Scan_Items_Scan_1` FOREIGN KEY (`Scan_ID`) REFERENCES `Scan` (`Scan_ID`);
ALTER TABLE `Scan_Items` ADD CONSTRAINT `fk_Scan_Items_Item_1` FOREIGN KEY (`Item_ID`) REFERENCES `Item` (`Item_ID`);

