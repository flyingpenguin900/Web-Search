# For documentation please add a single "docstring" to all modules/functions that you wish to be added to the documentation

```python
#This is a standard comment
def squareThis(x):
    """
    This is documentation for the function squareThis
    This function will return the square of a given value
    Example:
    ``
    >>>squareThis(10)
    10
    ``
    """
    return x*x

```
