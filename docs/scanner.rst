How to Make and Deploy Scanners
====================================

In this section, we are going to discuss how a developer should go about generating a new scanner to add to the project.
Due to the way that the project was built, adding a scanner to the system is relatively straightforward. To create a new 
scanner developers are encouraged to use whichever language they feel most confortable with;
however, there are a few restrictions on which languages can or should be used. The only restriction on which language
can be used is whether or not the language supports the RabbitMQ_ messaging protocol. All communication between modules
is handeled by this protocol, so the language used must support it. Additional considerations concerning which language
to use come from which marketplace a scanner is targeting. If the markeplace provides an api to leverage in order to
get results, then using a language that is already supported by the api would decrease development complexity. If the
marketplace does not provide an api then the developer should use whichever language they feel most comfortable scraping
and navigating webpages with.


.. _RabbitMQ: http://www.rabbitmq.com/


Making Scanners
---------------

Scanner Lifecycle
+++++++++++++++++
A scanners is small scripts whose only purpose should be to receive some products from the control engine. 
Then they should search their designated marketplace for these products. Finally they should
return what they find to the filter.


Receiving Search Terms
++++++++++++++++++++++
In order to know which products to search for scanners retrieve messages from the controller. Each message some metadata
that a scanner should pass back to the filter. This metadata is used by the database to keep track of scanners. The only
section that matters to a scanner is the 'Product Name' section. This field contains a list of all names that the are
tied to the product. This allows a scanner to search for a product and any of its pseudonyms, or nicknames. The format of
messages that a scanner will receive is illustrated below.

Scanners must accept JSON data of the form

.. code-block:: javascript

    {
        'Scanner_ID' : 123,
        'Event_ID' : 123,
        'Scan_ID' : 123,
        'Item_ID' : 123,
        'Alias_ID' : None,
        'Product Name' : ['name1', 'name2']
    }





Obtaining Results
+++++++++++++++++

After a scanner receives a message, it should then begin searching for that product on its target marketplace. The details
of implementation for this are largely marketplace specific, and are left up to the developer to decide. You can see how this
was done in the ebay scanner that was developed with the project.

Note: Scanners have the option of filtering their results prior to passing back to the
filter, if desired, but that is covered in the filtering section of the documentation.


Sending Results
+++++++++++++++
Now we need to discuss how a scanner should pass results on to the filtering service. This is message is largely the same
as the one received by the scanner. The message sent by the scanner should contain all products that it found in a list
identified by 'Items'. The message should also pass on all metadata that it received as well as the product name. This is
illustrated below.


Scanners must return JSON data of the form

.. code-block:: javascript

    {
        'Scanner_ID' : 123,
        'Event_ID' : 123,
        'Scan_ID' : 123,
        'Item_ID' : 123,
        'Alias_ID' : None,
        'Product Name' : ['name1', 'name2'],
        'Items' :
            [
                {
                    'Name' : 'Title of listing',
                    'URL' : 'Product URL',
                    'Category' : 'Primary category from e-bay listing'
                },
                {
                    'Name' : 'Title of listing',
                    'URL' : 'Product URL',
                    'Category' : 'Primary category from e-bay listing'
                }
            ]
    }


Deploying Scanners
------------------
In order to deploy scanners the must be added to the scan directory of the project. Currently there is only the ebay scanner there.
Then they must be added to the user interface so that user can select them when setting up a scan.