Scan Plugin/Engine/Service
==========================

* :ref:`ebay`

Scanners currently must accept JSON data of the form

.. code-block:: javascript

    {
        'Scanner_ID' : 123,
        'Event_ID' : 123,
        'Scan_ID' : 123,
        'Item_ID' : 123,
        'Alias_ID' : None,
        'Product Name' : ['name1', 'name2']
    }

Scanners must return JSON data of the form


.. code-block:: javascript

    {
        'Scanner_ID' : 123,
        'Event_ID' : 123,
        'Scan_ID' : 123,
        'Item_ID' : 123,
        'Alias_ID' : None,
        'Product Name' : ['name1', 'name2'],
        'Items' :
            [
                { 
                    'Name' : 'Title of listing',
                    'URL' : 'Product URL',
                    'Category' : 'Primary category from e-bay listing'
                },
                {
                    'Name' : 'Title of listing',
                    'URL' : 'Product URL',
                    'Category' : 'Primary category from e-bay listing'
                }
            ]
    }

.. _ebay:

Ebay Scanner
------------
.. automodule:: scan.eBayScanner
   :members:
   :undoc-members: