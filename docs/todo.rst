Upcoming Features
==============================

General plans and todo
--------------------------

* When one thread crashes in control, don't just keep running the others. Add in some kind of recovery plan.
* Find some way to detect if different parts of the scanner are running via python. Perhaps everything is its own Linux service, a RabbitMQ queue that broadcasts I am running (requires this thread to crash with the rest), or best of all: Maybe there is a way to see if a queue in RabbitMQ is being consumed from.
* Better organize queue consumption.
* At current the filter_results.py sits and collects everything out of a queue until it gets an entry that says its gotten everything. This Is Horrible. It comes from a problem with pika crashing when you attempt to send to much data from ebayscanner. Since queues allow for routes maybe the solution is to have every scan put all results (one at a time) into a unique route. Once this is done, pass along that route ID via the standard reception route.
* Current every result gets it own SQl entry, even if its the same result. The database supports listing what scans/scan events found a scan.
* **Important:** obfuscate DB and rabbitMQ with libraries. For example there is currently a rabbitMQ library, but it just makes interacting with rabbitMQ easier. Instead there should be a function that is "def scan_for_items(scanner, items)" which deals with making sure the scanner is running, connecting to rabbitMQ and sending data. Within that function there should maybe be just "def send_to_scanner(items)". Really make it so you only need to know how something is done if you go into the fuction that does that.
* **IMPORTANT:** When people where testing, they made a scan, and an update was sent to control. Control loged receiving this message with "b\'{"nothing":"nothing"}\'']". But the scan didn't happen. Nothing else in logs... why not? (I have added logging on both sides of aquiring lock. Hopefully this will help debug).
* **Documentation**
    * Update usage documentation (Note: Annotate screenshots with details and what to put where)
    * Update documentation landing/about page
    * Write documentation for RabbitMQ page
    * update documentation for code architecture
    * write documentation for design
    * write documentation for making/deploying scanners
    * Documentation is split into TONS of super tiny .rst files, see if that can be cleaned.
* **Site**
    * fix time zone issues. can the site auto convert entered times from user time to sql server time?
    * editing a schedule gives blank screen?
    * **History:**
        * Simple indicator that scan successfully ran. Make it obvious if the total results found is zero
        * Click scan to expand and see by item (alias?)
    * Store result feedback
    * Admin panel should have an update thing. Maybe a git style that basically does a git pull.
    * Add info on RabbitMQ queues/server to admin panel. This could be a good way for people to find problems, especially if we use it more properly.
    * add ? next to fields and buttons on website, hovering over them should give tips/help/info
    * Add ability to download different ".log" raw files from admin concole.
* Logs
    * Logs are saved in this format: Date Time - Who - Type - Message. They should be saved as JSON format.
    * Logs are read by the website via RE, if logs are switched to JSON, they should be read using JSON->Dict.
* scans
    * ebayscanner
        * Update ebay scanner to use modules.rabbitmq