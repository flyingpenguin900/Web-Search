RabbitMQ
==========================
RabbitMQ is a messaging protocol/service that allows messaging between programs. It is one of the worlds largest open
source message protocols, and is an industry standard. Our project utilizes it to allow our modules to communicate with eachother.
As discussed in other sections of the documentation, we needed to ensure that our modules could communicate with eachother
regardless of whether or not they were on the same machine. Using RabbitMQ allows us to ensure all modules can communicate
regardless of the underlying hardware.


How RabbitMQ Works
------------------
In general modules that want to utilize RabbitMQ establish a connection to the RabbitMQ service running on the local machine. Then
the module connects to a channel. Each module can then connect to q


Note
----
One thing to note about using RabbitMQ is that it does not support passing JSON strings. It does however support passing
strings. This is was minor issue since all modules expect to receive JSON formatted information. Thus some special care
needs to be taken on each end of a message. When a module wishes to send a message it must convert it the JSON string into
a generic string.

This can be done in python like so::

    import json
    json.dumps(message)
    #where message is the JSON formatted string you which to send.

On the other side, a module that wishes to receive a message must do two things. The messages are sent as binary strings, so
the must be encoded. Then the module must convert the string to a JSON formatted string.

This can be done in python like so::

    import json
    # body is the message received
    if isinstance(body, bytes):
        b_string = body
        string = b_string.decode("utf-8", "strict")
    else:
        string = body
    # use loads to convert the byte string to a JSON formatted string
    prod_as_dict = json.loads(string)  # loads converts 'string' to a JSON

    # then use dumps to convert the JSON to a python dictionary.
    item = json.dumps(prod_as_dict)


More information about RabbitMQ_ can be found on their website.
.. _RabbitMQ: http://www.rabbitmq.com/


Although it is mentioned in each respective section, below we provide information about general information about how each
module communicates with another.

Messaging (Control -> Scanner)
------------------------------
The control module sends messages to scan



Messaging (Ebay -> Filter)
--------------------------

