
Usage
=============================

.. _`Deployed Site`: http://senior.smeyer.xyz

Getting Started
---------------

Visit:
    * `Deployed Site`_
    * :ref:`Local_Installation`

You should see something like this

.. figure:: img/home.png

    Home page of app.

Add Some Search Items
---------------------

|pic3|  |pic4|

.. |pic3| image:: img/search_items.png
    :width: 49%

.. |pic4| image:: img/add_items.png
    :width: 49%

* Search Items Page
* Add Item Page

Schedule Yourself A Scan
------------------------

|pic1| |pic2|

.. |pic1| image:: img/schedule.png
    :width: 49%

.. |pic2| image:: img/schedule_new.png
    :width: 49%

* Schedule Page
* Schedule New Scan Page 

See Scan History
----------------

.. figure:: img/history.png

    Scan History Page

See Your Results
----------------

.. figure:: img/results.png

    Scan Results Page

