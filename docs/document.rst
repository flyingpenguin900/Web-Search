How To Document Your .py Files
====================================

Guide to documenting code for Web Scanner Project

Getting Started
------------------

All documentation is done with RST (reStructuredText).\n
Here are some great RST guides:

* http://docutils.sourceforge.net/rst.html
* http://docutils.sourceforge.net/docs/user/rst/quickref.html
* https://pythonhosted.org/an_example_pypi_project/sphinx.html (also a guide for sphinx doc which is what we use)

py file or module
-----------------

At the top of every module, you need a doc string::

    '''
    this is a doc string

    Heading!
    --------

    '''

All docstrings for our documentation need to end with a empty line (no tabs). 

This docstring will appear above all of your function documentation for the module.

Classes and Functions
---------------------

Like a module, classes and functions should begin with a docstring.

Unlike modules, there are a few special things you can do to define parameters and outputs.

This docstring in the function::

    def format_exception(etype, value, tb[, limit=None]):
        '''
        Format the exception with a traceback.

        :param etype: exception type
        :param value: exception value
        :param tb: traceback object
        :param limit: maximum number of stack frames to show
        :type limit: integer or None
        :rtype: list of strings
        '''
        actual function definition here.


would make this output

.. function:: format_exception(etype, value, tb[, limit=None])

   Format the exception with a traceback.

   :param etype: exception type
   :param value: exception value
   :param tb: traceback object
   :param limit: maximum number of stack frames to show
   :type limit: integer or None
   :rtype: list of strings

