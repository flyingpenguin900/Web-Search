About Web Scanner
==========================

Welcome to the Medtronic WebScanner Tool. This project was developed by Scott Meyer, Rachel Popo, and Alex Foreman for
their Senior Design project at the University of Colorado Denver. The project was developed over the course of two
Semesters from Fall 2017 through Spring 2018. The tool was developed for Medtronic, with Benjamin Pope as the client.
Additionally Jeaninne Looney and Matthew Vorhees at Medtronic were very helpful during the development of the project.

Project Goal
------------

Originally the project proposal was to build a univeral tool to find Medtronic project listings from dark web sites.
After examining and considering our options our client and the team decided to take a different approach. The main
reason for this comes from the fact that the "dark web" is not accessible in a practical way. For example, as we
understand it, the "dark web" is usually independently owned servers that don't typically use standard access protocols.
Typically they must be accessed through TOR browsers, and one must have some contact on the inside to be allowed access.
Furthermore the "dark web" can be a fairly dangerous place. Ultimately our client and the group decided that while it is
possible to do, our time would be better spent building a framework that the client could extend to the "dark web" at a
later date.

Project Concerns
----------------

Our client had a few main concerns that the team kept in mind during development. The first concern was to build the
project in a way that allowed it to scale as demand for resources increased. To do this we designed the whole project to
be entirely modular. We built 4 separate modules each addressing one portion on the problem. Each module communicates
via the industry standard messaging protocol RabbitMQ. This allows new modules to be added, and old modules to be changed
without changing the any others. With the modules being completely independant, the framework allows the modules to be
run on separate machines so that they can utilize additional resources. To accomplish this some logic would need to be
added to the messaging to inform each module to where they should publish messages, and from where they should recieve
messages. A suprisingly benificial side effect came from the modularity of the framework. Because the scanners are
independant from the remainder of the framework the can be developed in any language that supports the RabbitMQ messaging
protocol. This means that developers are free to use whichever language they are most comfortable using to add
additional scanners to the framework.


Another concern that Ben wanted considered was difficulty of maintainence. He wanted the project to be able to be handed
off to future developers with the least amount of difficulty possible. To address this we did two major things whilst
developing the project. Way back in the planning phase of development, we decided to develop the entire project in
Python. Our reasoning for doing this was to keep the coding language simple for future developers. For those
who may be unfamiliar with Python, in general it is a extremely popular scripting language. Its popularity comes from
it being easy to write and understand, even for those who may not have a programming background. This would allow whomever
picked up the project after us to quickly become familiar with what we did. The second thing we did to ensure a smooth
hand of was extensive documentation. We provided user and developer documentation, suggestions for improvement with
information relating to how to implement it, current bugs and ideas for how to fix, and as much information as we felt
would be helpful for future developers.


One of the most difficult aspects of the project came from the problem of filtering results that were not relevant.
Right from the start Ben wanted us to ensure that we were able to "find the needles in a haystack". This meant not only to
find relevant results from a massive amount of irrelevant results, but also to ensure we aren't losing relevant results
whilst processing irrelevant ones. This is suprisingly easy when the site you are searching provides an api that will
do this for you; however, in order to make the framework universal we couldn't rely on all scanning tools to pre-process
junk. This proved to be a problematic issue that was only partially solved in the current project. Essentially the current
scanner passes back all results and the filter only publishes a percentage of them to the database. This allows the filter
to exclude most of the likely irrelevant results without losing relevant ones. What is important is that the filter is
completely independant from the scanners and can be updated separately. In the filtering section of the documentation, we
will discuss a better way of filtering that we looked into. Ultimately we unfortunately ran out of time to finish
implementation of the filtering.