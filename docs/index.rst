.. Web Scanner documentation master file, created by
   sphinx-quickstart on Tue Oct 31 15:27:28 2017.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.


Web Scanner's documentation
===========================

This is documentation of the Web-Scanner Senior Design project.

Feel free to contribute at https://gitlab.com/flyingpenguin900/Web-Search/


.. _Usage:

.. toctree::
    :maxdepth: 4
    :caption: Usage

    about
    usage
    

.. _Project Administration:

.. toctree::
    :maxdepth: 4
    :caption: Project Administration

    install_setup
    scanner
    rabbitmq
    design
    code_architecture
    todo

.. _Code Documentation:

.. toctree::
    :maxdepth: 4
    :caption: Code Documentation

    flask
    control
    filter_results
    scan
    test
    modules


.. toctree::
    :maxdepth: 4
    :caption: document your code

    document
