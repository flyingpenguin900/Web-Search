"""
This is the logging module.

<HERE ARE INSTRUCTIONS FOR USE AND LOGGIN IN FILES... Wish I knew what I was doing.>
https://stackoverflow.com/questions/7621897/python-logging-module-globally

.. moduleauthor:: Scott Meyer <scott.r.meyer@gmail.com>
"""
import os, json, logging.config

def setup_logging(
    default_path='logging.json',
    default_level=logging.INFO,
    env_key='LOG_CFG'
):
    """Setup logging configuration

    """
    path = default_path
    value = os.getenv(env_key, None)
    if value:
        path = value
    if os.path.exists(path):
        with open(path, 'rt') as f:
            config = json.load(f)
        logging.config.dictConfig(config)
    else:
        logging.basicConfig(level=default_level)