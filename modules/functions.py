"""
This module contains functions which need to be included and used in multiple places.

.. moduleauthor:: Scott Meyer <scott.r.meyer@gmail.com>
"""
import os, json, logging.config
from modules import rabbitmq

def setup_logging(
    default_path='logging.json',
    default_level=logging.INFO,
    env_key='LOG_CFG'
):
    """Setup logging configuration

    """
    path = default_path
    value = os.getenv(env_key, None)
    if value:
        path = value
    if os.path.exists(path):
        with open(path, 'rt') as f:
            config = json.load(f)
        logging.config.dictConfig(config)
    else:
        logging.basicConfig(level=default_level)

def get_health():
    health = {
        'receive_from_site' : True,
        'Results_Filter' : True,
        'Ebay_Scanner' : True
    }

    

    return health