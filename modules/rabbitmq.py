'''
A lot of different files need to interact with RabbitMQ
This module is an attempt to centuralize all dealings with RabbitMQ so changes are easier


TODO: Use this class everywhere someone talks to rabbitMQ (scan, flask)
TODO: Make the class more modular, and learn more about rabbitMQ to add more features to the class.

.. moduleauthor:: Scott Meyer <scott.r.meyer@gmail.com>

'''

import pika, logging

#TODO: documentation
#TODO: diseminate this into: control.py ebayscanner.py, filter_results.py, flask.py
class rabbit:
    """
    This class is used whenever something has to be done with the rabbitMQ system.

    Options include the in queue (consumed), the out queue (published to), and the server address.

    General external uses:

    __init__(who, in_queue=None, out_queue=None, server='127.0.0.1')
    publish(q)
    consume_queue_items
    close
    """

    r_server = ''
    logger = logging.getLogger("rabbit")

    # To change the where status messages are sent, change this function
    status_q = "status"

    # Default in and out queue's for different processes.
    #Please note, Named is based on pushing, not consuming.
    qs = {
        'site' : 'site',
        'control' : 'control',
        'ebay' : 'results_queue'
    }

    #TODO: options, including server, connection info, how much to return, and so on.
    def __init__(self, who, in_queue=None, out_queue=None, server='127.0.0.1'):
        """This function is ran on creation of an instance of the rabbit class"""
        self.who = who
        self.rabbitMQ = pika.BlockingConnection(pika.ConnectionParameters(host=server))
        self.r_server = server
        self.logger = logging.getLogger("rabbit." + who)

        if in_queue is None:
            if who in self.qs:
                self.set_consume_queue(self.qs[who])
            else:
                self.set_consume_queue(who)
        else:
            self.set_consume_queue(in_queue)

        if out_queue is None:
            if who in self.qs:
                self.set_publish_queue(self.qs[who])
            else:
                self.set_publish_queue(who)
        else:
            self.set_publish_queue(out_queue)
    
    def set_consume_queue(self, q):
        self.consume_queue = q
        self.consume_channel = self.rabbitMQ.channel()
        self.consume_channel.queue_declare(queue=self.consume_queue)

    def set_publish_queue(self, q):
        self.publish_queue = q
        self.publish_channel = self.rabbitMQ.channel()
        self.publish_channel.queue_declare(queue=self.publish_queue)

    #take String(message) push to Queue(out_q)
    def publish(self, message):
        """Given a text message, push the message to `publish_queue`"""
        self.publish_channel.basic_publish(
            exchange='',
            routing_key=self.publish_queue,
            body=message
        )

    def consume_queue_items(self, f):
        '''Consume from self.consume_queue with given function

        :param queue: RabbitMQ queue to consume over.
        :param recheck: Every time an item enters the queue, this function will be ran with the body passed through
        :type queue: string 
        :type function: function(string)
        :rtype: None

        #TODO: This here is a messy solution to getting different messages though a queue. RabbitMQ has its own ways to handle this. Fix.
        '''

        #This is the fuction that will be consumed over
        def consume(ch, method, properties, body):
            if isinstance(body, bytes):
                body = body.decode('utf8')

            if body.lower() == "status-check":
                self._status_reply()
            else:
                f(body)

        self._consume(consume)

    def close(self):
        """Close the rabbitMQ server connection"""
        self.rabbitMQ.close()


    #### These functions are mainly internal.


    def push_to(self, w_queue, message):
        """Give a queue, and a message. Push that message to the queue"""
        out_q = self.rabbitMQ.channel()
        out_q.queue_declare(queue=w_queue)
        out_q.basic_publish(
            exchange='',
            routing_key=w_queue,
            body=message
        )

    def _status_reply(self):
        self.push_to(self.status_q, self.who)

    #take Queue(q), and consume it with Function(func)
    def _consume(self, func, q=None, do_no_ack=True):
        """Consume over a queue with provided function(ch, method, properties, message)
        If a "q" is provided, consume over that queue, otherwise consume over the class
        in_q.
        """
        if q is not None:
            self.logger.info('Going to consume from queue: %s' % q)
            channel = self.rabbitMQ.channel()
            channel.queue_declare(queue=q)
            channel.basic_consume(
                func,
                queue=q,
                no_ack=do_no_ack
            )
            channel.start_consuming()
        else:
            self.logger.info('Going to consume from queue: %s' % self.consume_queue)
            self.consume_channel.basic_consume(
                func,
                queue=self.consume_queue,
                no_ack=do_no_ack
            )
            self.consume_channel.start_consuming()

def consume_from_queue(q, f):
    r = rabbit(q)
    r.consume_queue_items(f)
    r.close()

def push_to_queue(q, s):
    r = rabbit(q)
    r.publish(s)
    r.close