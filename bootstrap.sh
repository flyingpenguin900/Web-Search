#!/usr/bin/env bash

echo -e "\n--- Mkay, installing now... ---\n"

echo -e "\n--- Updating packages list ---\n"
sudo apt-get -qq update

echo -e "\n--- Install base packages ---\n"
sudo DEBIAN_FRONTEND=noninteractive sudo -E apt-get -q -y install  apache2 rabbitmq-server python3-pip libmysqlclient-dev >> /vagrant/vm_build.log 2>&1

echo -e "\n--- Install python packages ---\n"
sudo pip3 install --upgrade pip >> /vagrant/vm_build.log 2>&1
sudo pip3 install pika >> /vagrant/vm_build.log 2>&1
sudo pip3 install psycopg2 sqlalchemy mysqlclient >> /vagrant/vm_build.log 2>&1
sudo pip3 install Flask flask-mysql flask-session pytz sphinx sphinx_rtd_theme recommonmark >> /vagrant/vm_build.log 2>&1
echo -e "\n--- Done. ---"

# clear apache www folder, and link to /vagrant
if ! [ -L /var/www ]; then
    rm -rf /var/www
    mkdir /var/www
    mkdir /vagrant/docs/_build
    mkdir /vagrant/docs/_build/html
    ln -fs /vagrant/docs/_build/html /var/www/html
fi

echo -e "\n--- Begin Alex Install ---\n"
sudo apt-get install -y python-lxml >> /vagrant/vm_build.log 2>&1
sudo apt-get install -y python-setuptools >> /vagrant/vm_build.log 2>&1
sudo pip3 install ebaysdk >> /vagrant/vm_build.log 2>&1
sudo apt-get install -y libapache2-mod-wsgi python-dev >> /vagrant/vm_build.log 2>&1
cd /vagrant/
sudo sudo a2enmod wsgi >> /vagrant/vm_build.log 2>&1
cd ~
sudo pip3 install virtualenv >> /vagrant/vm_build.log 2>&1
cd /
sudo virtualenv WebscannerEnv >> /vagrant/vm_build.log 2>&1
cd vagrant/
source ../WebscannerEnv/bin/activate >> /vagrant/vm_build.log 2>&1
sudo pip3 install Flask flask-mysql >> /vagrant/vm_build.log 2>&1
deactivate
cd ../
sudo cp /vagrant/FlaskApp/FlaskApp/FlaskApp.conf  /etc/apache2/sites-available/FlaskApp.conf

sudo a2ensite FlaskApp >> /vagrant/vm_build.log 2>&1
sudo service apache2 restart >> /vagrant/vm_build.log 2>&1
sudo service apache2 reload >> /vagrant/vm_build.log 2>&1
echo -e "\n--- Alex Install Done. ---\n"


# lets get permissions
sudo chown -R ubuntu /vagrant
sudo chmod -R +x /vagrant

# Setup website as a service
cat > /etc/systemd/system/medtronicscanner-flask.service << EOL
#!/bin/sh
[Unit]
Description=MedtronicScanner project website
After=network.target

[Service]
User=ubuntu
RemainAfterExit=Yes
ExecStart=/usr/bin/python3 /vagrant/FlaskApp/FlaskApp/__init__.py
WorkingDirectory=/vagrant/FlaskApp/FlaskApp
Restart=Always

[Install]
WantedBy=multi-user.target
EOL

# Setup control as a service
cat > /etc/systemd/system/medtronicscanner-control.service << EOL
#!/bin/sh
[Unit]
Description=MedtronicScanner contol
After=network.target

[Service]
User=ubuntu
RemainAfterExit=Yes
ExecStart=/usr/bin/python3 /vagrant/control/control.py
WorkingDirectory=/vagrant/control
Restart=Always

[Install]
WantedBy=multi-user.target
EOL

# Setup filter as a service
cat > /etc/systemd/system/medtronicscanner-filter.service << EOL
#!/bin/sh
[Unit]
Description=MedtronicScanner filter
After=network.target

[Service]
User=ubuntu
RemainAfterExit=Yes
ExecStart=/usr/bin/python3 /vagrant/control/filter_results.py
WorkingDirectory=/vagrant/control
Restart=Always

[Install]
WantedBy=multi-user.target
EOL

#Enable and run service
systemctl enable medtronicscanner-flask
systemctl start medtronicscanner-flask
apachectl start

#if amazon there is a passed permater
if ! [ -z $1 ]; then
    systemctl enable medtronicscanner-control
    systemctl start medtronicscanner-control

    systemctl enable medtronicscanner-filter
    systemctl start medtronicscanner-filter
fi
