
# ---- Begin SQL Demo ---- #
#Connect to and query posgreSQL on AWS
#postgreSQL package
import psycopg2

#create connection to database
conn = psycopg2.connect(host="web-scanner.ckdobmxcyuc7.us-east-2.rds.amazonaws.com"
                      , database="test"
                      , user="seniordesign"
                      , password="0XQkMbXu9A1d")

#define a cursor on the database connection,
cur = conn.cursor()

#Run a SQL query
cur.execute("SELECT * FROM test_table;")

#return all SQL query results in a list
print(cur.fetchall())

#close connections
cur.close()
conn.close()
# ---- End SQL Demo ---- #
# ---- Begin rabbitMQ  Demo ---- #

import pika
import json

# Create a connection to the local rabbitMQ server
connection = pika.BlockingConnection(pika.ConnectionParameters(host="127.0.0.1"))

# Declare a channel to work with
channel = connection.channel()
channel.queue_declare(queue="search")

# Create a json "message"
data = {
    "id" : 1,
    "name" : "My Name",
    "description" : "This is description about me"
}
message = json.dumps(data)

# Send data to rabbitMQ channel
channel.basic_publish(exchange=''
                    , routing_key="search"
                    , body=message)
print(" [x] Sent data to RabbitMQ")
connection.close()
# ---- End rabbitMQ Demo ---- #

import subprocess
# subprocess.Popen
# This is an attempt to start up a python script and keep our eyes on it
pid = subprocess.Popen(["python3", "service_example.py"])

