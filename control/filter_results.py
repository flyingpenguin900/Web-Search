"""
This file filters the results returned by the scan engine.
At present, it takes a results queue & filtered_results queue as a startup option
If either are blank, defaults to "results" & "filtered_results"


TODO: Proper adding of items to database
TODO: Proper rate_item
TODO: Better testing options (maybe something that auto puts stuff in a different results queue thats for testing?)
TODO: Item rate_item doesn't account for capitals

.. moduleauthor:: Scott Meyer <scott.r.meyer@gmail.com>
"""

import sys
sys.path.append('/vagrant')

from modules.functions import setup_logging
from modules import rabbitmq
from math import ceil
import json, datetime, sys, getopt, logging

debug = False
bash = False
alt_input = False
consume_queue = 'results_queue'
logger = logging.getLogger("filter_results")

# Items(dict), productname(String) -> Items(dict)(Sorted)


def item_sort(items, pname):
    """return the items list, sorted based on the rating received from rate_item()

    :param items: A list of items returned by a scan.
    :param pname: The name of the product searched for.
    :type scan: list of dictionaries
    :type pname: strings
    :rtype: List of Dictionaries (sorted)
    """
    if bash:
        print(list(map(
                        lambda item: rate_item(item, pname),
                        items)))
    return list(map(
        lambda x: x[1],
        sorted(
            list(zip(
                map(
                    lambda item: rate_item(item, pname),
                    items), 
                items)), 
            key=lambda x: x[0],
            reverse=True)))

# Given an item(dict), return a numerical rating.


def rate_item(item, pname):
    """Given an Item (Dict) from a scan result, give it a numerical rating.

    Do this by turning it into one long string and counting the occurances of a word
    to do this, we need to join all values of the dict seperated by spaces, then split everything by spaces
    then filter for a specific word (or group of words?) then get a length of said list
    """
    return len(list(filter(
        lambda x: x in pname,
        ' '.join(list(item.values())).split(' '))))

# TODO: Fix this up


def add_item(item, data):
    """Add an item to the database"""
    Added_Item = {
        'Item_ID' : data['Item_ID'],
        'Title' : item['Name']
    }
    logger.debug("Adding %s to the results database table." % Added_Item)
    orm.add(db.Results(
        Item_ID = data['Item_ID'],
        Alias_ID = data['Alias_ID'],
        Scan_ID = data['Scan_ID'],
        Event_ID = data['Event_ID'],
        Scanner_ID = data['Scanner_ID'],
        URL = item['URL'],
        Title = item['Name'],
        Date_Posted = None,
        Data = None,
        Date_Found = datetime.datetime.now()
    ))
    orm.commit()


def process_result(body):
    """

    This is the core of filter_results.
    This is the function that gets automatically ran when scan results come in through the queue.

    Scan results come as a json object:
    With all the info needed for an item scan, as well as a list of "Items" that are the results.

    Sort all of those items, and add the first 10 to the database.

    Body messages come in this form:
    {
        'URL' : 'URL String'
        'Alias_ID' : 'empty'
    }

    TODO: Don't have the first 10 hardcoded. This should eventually be a setting.
    TODO: Ability to have only the best show up on all results, but users can see more on the specific scan.

    """

    if isinstance(body, bytes):
        data = json.loads(body.decode('utf8'))
    else:
        data = json.loads(body)


    logger.info(
        "Running with: " 
        + str(len(data['Items']))
        + " Items. Event ID: "
        + str(data['Event_ID'])
        + " Item_ID: "
        + str(data['Item_ID'])
        )
    #We have a list of items
    #we need to find some way to order/arange the list by likelyhood of being a correct responce
    sorted_items = item_sort(data['Items'], data['Product Name'])
    #now we need to add the top 10 to the database as results
    tenpercent = ceil(len(data['Items']) * 0.05)
    sorted_items[:tenpercent]
    
    for i, item in zip(range(10), sorted_items):
        if not bash:
            add_item(item, data)
        else:
            print("ADDING THIS ITEM:")
            print(item)


# If this file isn't imported, run main.
if __name__ == '__main__':
    # Deal with ability to set a testing arguement when starting
    argv = sys.argv[1:]
    try:
        opts, args = getopt.getopt(
            argv, 
            "db", 
            ["debug","bash","input_file=","consume_queue="])
    except getopt.GetoptError:
        # if not options run the file using the inputqueue as the requests_queue
        print('Invalid options selected.')
        sys.exit()
    for opt, arg in opts:
        if opt in ('-d','--debug'):
            debug = True
        if opt in ('-b','--bash'):
            bash = True
        if opt in ('--input_file'):
            alt_input = arg
        if opt in ('--consume_queue'):
            consume_queue = arg
        
    

    #TODO: Git rid of this, requires fixing add_item
    import database.orm as db
    from sqlalchemy.orm import sessionmaker
    Session = sessionmaker(bind=db.engine)
    orm = Session()

    if debug:
        setup_logging(default_level=logging.INFO)
    else:
        setup_logging(default_level=logging.DEBUG)

    #Setup rabbit mq, pass process result as function for incoming message in queue
    if not alt_input:
        #setup rabbitmq
        rabbitmq.consume_from_queue('ebay', process_result)
    else:
        process_result(0, 0, 0, open(alt_input,'r'))
