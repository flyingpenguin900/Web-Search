'''
This is the testing suite for the webscanner.

Using different starting options change which tests are ran.

Multiple tests exist for each aspect of the project, as well as tests that include multiple aspects.
Development of the testing suite is ongoing, so check back often for new features.

Python modules wishing to support the test_suite should support the following options:
    * '--bash' This means output that normally goes somewhere (like rabbitmq or sql) should go to bash
    * '--input_file=something' This means that instead of getting input from somewhere (like rabbitmq or sql) input should come from the specified file
    * '--consume_queue=something' This prodives a specific rabbitMQ queue to consume messages from
    * '--consume_count=2' This is the amount of times to consume from the queue before sys.exit()
    * '--push_queue=something' This provides a specific rabbitMQ queue to push items to

* TODO: Looking into docopt (https://github.com/docopt/docopt) world suite this WAY better.
* TODO: Control testing
* TODO: scanner testing
* TODO: flask/webserver/site testing
* TODO: More comprehensive filter_results testing.
* TODO: output compair files, ability to have a pass/fail for checking on code updates


Run Options
================
* --help:   Help with running this python module.

* --version:    Current version of this module.

* --filter: Include filter in testing.

.. moduleauthor:: Scott Meyer <scott.r.meyer@gmail.com>

'''
version = "0.01a"
help_message = """
This is the test suite. Someone should make a help message
"""
import subprocess, getopt, sys
from time import sleep

class test_suite:
    """This is the test suite class.
    After (or during) creation, set permeters for tests
    then run run() to run the tests you setup
    """
    filter = False

    def run(self):
        """Run all tests set to True in the test_suite object instance"""
        if filter: self.filter_test()
        sleep(1)

    def print_update(self, message):
        """Given the string "yes" print "===== yes ====="."""
        print("===== " + message + " =====")

    def filter_test(self):
        """Run the test of filter_results.py"""
        self.print_update("Begin Filter Test")
        sleep(1)
        process = ['python3','../control/filter_results.py']
        options = [
            '--bash',
            '--input_file','../testing/filter_test_input.json']
        command = process + options
        subprocess.run(command)
        self.print_update("End Filter Test")
        

#If this file isn't imported run this:
if __name__ == '__main__':
    subprocess.run(['python3','test_test_suite.py'])
    print("hello World, Test suite begin")

    #make suite instance
    suite = test_suite()

    #Handle opening options (arguments) to setup for testing.
    try:
        opts, args = getopt.getopt(
            sys.argv[1:], 
            "hvf", 
            ["help", "version", "filter"])
    except getopt.GetoptError:
        # if not options run the file using the inputqueue as the requests_queue
        print('Invalid options selected.')
        sys.exit()
    for opt, arg in opts:
        if opt in ('-h', '--help'):
            print(help_message)
            sys.exit()
        elif opt in ('-v', '--version'):
            print(version)
            sys.exit()
        elif opt in ('-f', '--filter'):
            suite.filter = True
    suite.run()

    print(suite.filter)
