# Web Search 

This project uses [Vagrant](https://www.vagrantup.com/) to keep track of the environment for eventual implementation on AWS.

Vagrant requires a Virtual Machine engine, though all work, [VirtualBox](https://www.virtualbox.org/) is recomended.

To work with Vagrant open a CMD or Terminal window and browse to the main directory of the project

```sh
vagrant up        # This turns on the Virtual Machine
vagrant halt      # Shutdown the Virtual Machine
vagrant suspend   # Pause the Virtual Machine
vagrant resume    # Resume the paused Virtual Machine
vagrant destroy   # completely destroy the VM, you can use UP to re-initialize it


vagrant ssh    # remote (SSH) into the VM
```

# Update
There are two critical files for keeping track of the state of the Virtual Machine

## Vagrantfile

This file sets up settings of the Virtual Machine itself

There are a few critical parts to it, The first section forwards a port from inside the VM to outside the VM
```bash
#Below is an example of forwarding port 80 from the VM to 4567 to the host
config.vm.network :forwarded_port, guest: 80, host: 4567
```
The second part is a set of synced folders, these are folders you want to have on the host machine that are mounted to a specific point on the VM so that you don’t have to actually bring files between the VM and the host
```bash
# Below is an example of synching the folder the VM is started from, to the folder “/vagrant”
config.vm.synced_folder ".", "/vagrant", :mount_options => ["dmode=777", "fmode=666"]
```
## bootstrap .sh
This is basically a list of commands that need to be run to setup the environment.

If you need Python 3.0, and pip, then the python3 package “pika” you would add this to the file.

```bash
apt install python3 python3-pip
pip3 install pika
```
